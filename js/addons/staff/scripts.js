$(document).ready(function(){
	$(".team-slide ul").owlCarousel({
		itemsDesktop:[2000,7],
		itemsDesktopSmall:[1300,6],
		itemsTablet:[1150,4],
		itemsMobile:[767,1],
		navigation : true,
		pagination:false,
		navigationText : ['<i class="bx-prev icon-car-left"></i>','<i class="bx-next icon-car-right"></i>'],
		rewindNav : true,	 
		slideSpeed : 500,
		paginationSpeed : 800,
		rewindSpeed : 500,scrollPerPage : false,
	});
});