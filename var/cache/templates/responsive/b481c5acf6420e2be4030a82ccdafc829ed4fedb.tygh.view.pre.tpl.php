<?php /* Smarty version Smarty-3.1.21, created on 2015-11-30 01:10:33
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/books_data/hooks/categories/view.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1244198374565b77d9b8cb21-06621460%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b481c5acf6420e2be4030a82ccdafc829ed4fedb' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/books_data/hooks/categories/view.pre.tpl',
      1 => 1448630443,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1244198374565b77d9b8cb21-06621460',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'category_data' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565b77d9be9ef4_09227886',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565b77d9be9ef4_09227886')) {function content_565b77d9be9ef4_09227886($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('books_data','isbn','author','language','format','books_data','isbn','author','language','format'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['category_data']->value['isbn']||$_smarty_tpl->tpl_vars['category_data']->value['author']||$_smarty_tpl->tpl_vars['category_data']->value['language']||$_smarty_tpl->tpl_vars['category_data']->value['format']) {?>
<div class="info">
    <h3><?php echo $_smarty_tpl->__("books_data");?>
<h3>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['isbn']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("isbn");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['isbn'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['author']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("author");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['author'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['language']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("language");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['language'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['format']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("format");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['format'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
</div>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/books_data/hooks/categories/view.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/books_data/hooks/categories/view.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['category_data']->value['isbn']||$_smarty_tpl->tpl_vars['category_data']->value['author']||$_smarty_tpl->tpl_vars['category_data']->value['language']||$_smarty_tpl->tpl_vars['category_data']->value['format']) {?>
<div class="info">
    <h3><?php echo $_smarty_tpl->__("books_data");?>
<h3>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['isbn']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("isbn");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['isbn'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['author']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("author");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['author'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['language']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("language");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['language'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category_data']->value['format']) {?>
        <div class="ty-info-group">
            <span class="ty-control-group__label"><?php echo $_smarty_tpl->__("format");?>
:</span>
            <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['format'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
    <?php }?>
</div>
<?php }?>
<?php }?><?php }} ?>
