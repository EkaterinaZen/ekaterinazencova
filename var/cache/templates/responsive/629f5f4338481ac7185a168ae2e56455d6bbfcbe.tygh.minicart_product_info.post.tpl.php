<?php /* Smarty version Smarty-3.1.21, created on 2015-11-30 15:00:50
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/checkout/minicart_product_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:883150403565c3a72b45cb9-47593714%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '629f5f4338481ac7185a168ae2e56455d6bbfcbe' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/checkout/minicart_product_info.post.tpl',
      1 => 1446489755,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '883150403565c3a72b45cb9-47593714',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'product' => 0,
    '_cart_products' => 0,
    '_product' => 0,
    'key' => 0,
    'block' => 0,
    'dropdown_id' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565c3a72bfc4c6_90451327',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565c3a72bfc4c6_90451327')) {function content_565c3a72bfc4c6_90451327($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['product']->value['extra']['buy_together']) {?>
    <ul class="ty-buy-together-cart-items__list">
        <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["_key"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
            <?php if ($_smarty_tpl->tpl_vars['_product']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
                <li class="ty-buy-together-cart-items__list-item">
                    <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['products_links_type']=="thumb") {?>
                        <div class="ty-cart-items__list-item-image">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"40",'image_height'=>"40",'images'=>$_smarty_tpl->tpl_vars['_product']->value['main_pair'],'no_ids'=>true), 0);?>

                        </div>
                    <?php }?>
                    <div class="ty-cart-items__list-item-desc">
                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['_product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"
                           class="ty-buy-together-cart__item-link"><?php echo htmlspecialchars(fn_get_product_name($_smarty_tpl->tpl_vars['_product']->value['product_id']), ENT_QUOTES, 'UTF-8');?>
</a>
                        <p>
                            <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span><span>&nbsp;x&nbsp;</span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['display_price'],'span_id'=>"price_".((string)$_smarty_tpl->tpl_vars['key']->value)."_".((string)$_smarty_tpl->tpl_vars['dropdown_id']->value),'class'=>"none"), 0);?>
</p>
                    </div>
                </li>
                <?php if ($_smarty_tpl->tpl_vars['_product']->value['product_option_data']) {?>
                    <li class="ty-buy-together-cart__item"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['_product']->value['product_option_data']), 0);?>
</li>
                <?php }?>
            <?php }?>
        <?php } ?>
    </ul>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/buy_together/hooks/checkout/minicart_product_info.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/buy_together/hooks/checkout/minicart_product_info.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['product']->value['extra']['buy_together']) {?>
    <ul class="ty-buy-together-cart-items__list">
        <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["_key"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
            <?php if ($_smarty_tpl->tpl_vars['_product']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
                <li class="ty-buy-together-cart-items__list-item">
                    <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['products_links_type']=="thumb") {?>
                        <div class="ty-cart-items__list-item-image">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"40",'image_height'=>"40",'images'=>$_smarty_tpl->tpl_vars['_product']->value['main_pair'],'no_ids'=>true), 0);?>

                        </div>
                    <?php }?>
                    <div class="ty-cart-items__list-item-desc">
                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['_product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"
                           class="ty-buy-together-cart__item-link"><?php echo htmlspecialchars(fn_get_product_name($_smarty_tpl->tpl_vars['_product']->value['product_id']), ENT_QUOTES, 'UTF-8');?>
</a>
                        <p>
                            <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span><span>&nbsp;x&nbsp;</span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['display_price'],'span_id'=>"price_".((string)$_smarty_tpl->tpl_vars['key']->value)."_".((string)$_smarty_tpl->tpl_vars['dropdown_id']->value),'class'=>"none"), 0);?>
</p>
                    </div>
                </li>
                <?php if ($_smarty_tpl->tpl_vars['_product']->value['product_option_data']) {?>
                    <li class="ty-buy-together-cart__item"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['_product']->value['product_option_data']), 0);?>
</li>
                <?php }?>
            <?php }?>
        <?php } ?>
    </ul>
<?php }
}?><?php }} ?>
