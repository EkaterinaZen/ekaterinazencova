<?php /* Smarty version Smarty-3.1.21, created on 2015-11-30 07:15:12
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/products/tabs_extra.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:222466534565bcd505ea0f5-15493597%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c91089d901b123bca9c72efcde5c6fb22e2ebcb' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/products/tabs_extra.post.tpl',
      1 => 1441800576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '222466534565bcd505ea0f5-15493597',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'no_hide_input_if_shared_product' => 0,
    'hide_controls' => 0,
    'product_data' => 0,
    'chains' => 0,
    'chain' => 0,
    'link_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565bcd5064fd74_79654094',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565bcd5064fd74_79654094')) {function content_565bcd5064fd74_79654094($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('new_combination','add_combination','view','edit','editing_combination','no_data'));
?>
<?php if (fn_allowed_for("ULTIMATE")) {?>
    <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']&&!$_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value) {?>
        <?php $_smarty_tpl->tpl_vars["hide_controls"] = new Smarty_variable(false, null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["hide_controls"] = new Smarty_variable(true, null, 0);?>
    <?php }?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["hide_controls"] = new Smarty_variable(false, null, 0);?>
<?php }?>

<?php if (fn_allowed_for("MULTIVENDOR")) {?>
    <?php if (!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
        <?php $_smarty_tpl->tpl_vars["hide_controls"] = new Smarty_variable(true, null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["hide_controls"] = new Smarty_variable(false, null, 0);?>
    <?php }?>
<?php }?>

<div id="content_buy_together" class="cm-hide-save-button hidden <?php if ($_smarty_tpl->tpl_vars['hide_controls']->value) {?>cm-hide-inputs<?php }?>">
    <?php if (!$_smarty_tpl->tpl_vars['hide_controls']->value) {?>
        <div class="clearfix">
            <div class="pull-right">
                    <?php $_smarty_tpl->_capture_stack[0][] = array("add_new_picker", null, null); ob_start(); ?>
                        <div id="add_new_chain">
                            <?php echo $_smarty_tpl->getSubTemplate ("addons/buy_together/views/buy_together/update.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_id'=>$_smarty_tpl->tpl_vars['product_data']->value['product_id'],'item'=>array()), 0);?>

                        </div>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"add_new_chain",'text'=>__("new_combination"),'content'=>Smarty::$_smarty_vars['capture']['add_new_picker'],'link_text'=>__("add_combination"),'act'=>"general"), 0);?>

            </div>
        </div><br>
    <?php }?>
    
    <div class="items-container" id="update_chains_list">
        <table class="table table-middle table-objects">
        <?php if ($_smarty_tpl->tpl_vars['chains']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['chain'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['chain']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['chains']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['chain']->key => $_smarty_tpl->tpl_vars['chain']->value) {
$_smarty_tpl->tpl_vars['chain']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['hide_controls']->value) {?>
                    <?php $_smarty_tpl->tpl_vars['link_text'] = new Smarty_variable($_smarty_tpl->__("view"), null, 0);?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars['link_text'] = new Smarty_variable($_smarty_tpl->__("edit"), null, 0);?>
                <?php }?>

                <?php ob_start();
echo $_smarty_tpl->__("editing_combination");
$_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/object_group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>$_smarty_tpl->tpl_vars['chain']->value['chain_id'],'id_prefix'=>"_bt_",'text'=>$_smarty_tpl->tpl_vars['chain']->value['name'],'status'=>$_smarty_tpl->tpl_vars['chain']->value['status'],'hidden'=>false,'href'=>"buy_together.update?chain_id=".((string)$_smarty_tpl->tpl_vars['chain']->value['chain_id'])."&product_id=".((string)$_smarty_tpl->tpl_vars['chain']->value['product_id']),'link_text'=>$_smarty_tpl->tpl_vars['link_text']->value,'object_id_name'=>"chain_id",'table'=>"buy_together",'href_delete'=>"buy_together.delete?chain_id=".((string)$_smarty_tpl->tpl_vars['chain']->value['chain_id']),'delete_target_id'=>"update_chains_list",'header_text'=>$_tmp6.": ".((string)$_smarty_tpl->tpl_vars['chain']->value['name']),'skip_delete'=>$_smarty_tpl->tpl_vars['hide_controls']->value,'no_table'=>true,'hide_for_vendor'=>$_smarty_tpl->tpl_vars['hide_controls']->value), 0);?>

            <?php } ?>
        <?php } else { ?>
            <tr><td><?php echo $_smarty_tpl->__("no_data");?>
</td></tr>
        <?php }?>
        </table>
    <!--update_chains_list--></div>
<!--content_buy_together--></div><?php }} ?>
