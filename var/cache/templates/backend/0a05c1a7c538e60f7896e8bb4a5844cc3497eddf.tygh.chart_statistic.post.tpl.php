<?php /* Smarty version Smarty-3.1.21, created on 2015-11-29 23:06:44
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/orders_statistics/hooks/index/chart_statistic.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:753939575565b5ad438cfc1-82636492%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0a05c1a7c538e60f7896e8bb4a5844cc3497eddf' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/orders_statistics/hooks/index/chart_statistic.post.tpl',
      1 => 1447949003,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '753939575565b5ad438cfc1-82636492',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orders_statistics' => 0,
    'status' => 0,
    'four_statuses' => 0,
    'order_statistics' => 0,
    'gross_total' => 0,
    'total_paid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565b5ad441c445_57985334',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565b5ad441c445_57985334')) {function content_565b5ad441c445_57985334($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('status','this_week','previous_week','this_month','previous_month','this_year','previous_year'));
?>
<div id="content_orders_statistics">     
    <div id="dashboard_statistics_orders_statistics">  
        <div class="dashboard-table dashboard-table-orders_statistics">
            <div class="table-wrap" id="dashboard_orders_statistics">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="14%"><?php echo $_smarty_tpl->__("status");?>
</th>
                            <th width="13%"><?php echo $_smarty_tpl->__("this_week");?>
</th>
                            <th width="14%"><?php echo $_smarty_tpl->__("previous_week");?>
</th>
                            <th width="14%"><?php echo $_smarty_tpl->__("this_month");?>
</th>
                            <th width="14%"><?php echo $_smarty_tpl->__("previous_month");?>
</th>
                            <th width="13%"><?php echo $_smarty_tpl->__("this_year");?>
</th>
                            <th width="16%"><?php echo $_smarty_tpl->__("previous_year");?>
</th>                
                        </tr>
                    </thead>
                </table>
                <div class="scrollable-table">
                    <table class="table table-striped">
                        <tbody>
                            <?php  $_smarty_tpl->tpl_vars["order_statistics"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["order_statistics"]->_loop = false;
 $_smarty_tpl->tpl_vars["status"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['orders_statistics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["order_statistics"]->key => $_smarty_tpl->tpl_vars["order_statistics"]->value) {
$_smarty_tpl->tpl_vars["order_statistics"]->_loop = true;
 $_smarty_tpl->tpl_vars["status"]->value = $_smarty_tpl->tpl_vars["order_statistics"]->key;
?>                 
                                <tr>
                                    <td width="14%"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['four_statuses']->value[$_smarty_tpl->tpl_vars['status']->value], ENT_QUOTES, 'UTF-8');?>
</td>
                                    <td width="14%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['this_week']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['this_week']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>
                                    <td width="14%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['last_week']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['last_week']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>
                                    <td width="14%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['this_month']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['this_month']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>
                                    <td width="14%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['last_month']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['last_month']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>                                  
                                    <td width="13%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['this_year']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['this_year']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>
                                    <td width="16%">
                                        <?php if (($_smarty_tpl->tpl_vars['order_statistics']->value['last_year']['count'])) {?> 
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_statistics']->value['last_year']['count'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?>0
                                        <?php }?>
                                    </td>                                                     
                                </tr>
                            <?php } ?>
                            <tr> 
                                <td width="14%"><b><?php echo $_smarty_tpl->__('gross_total');?>
</b></td>                               
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['this_week'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['this_week']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['last_week'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['last_week']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['this_month'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['this_month']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['last_month'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['last_month']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="13%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['this_year'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['this_year']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="16%">
                                    <?php if (($_smarty_tpl->tpl_vars['gross_total']->value['last_year'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['gross_total']->value['last_year']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>                              
                            </tr>
                            <tr> 
                                <td width="14%"><b><?php echo $_smarty_tpl->__('totally_paid');?>
</b></td>                               
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['this_week'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['this_week']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['last_week'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['last_week']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['this_month'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['this_month']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="14%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['last_month'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['last_month']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="13%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['this_year'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['this_year']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>
                                <td width="16%">
                                    <?php if (($_smarty_tpl->tpl_vars['total_paid']->value['last_year'])) {?> 
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_paid']->value['last_year']), 0);?>
 <?php } else { ?>0
                                    <?php }?>
                                </td>                              
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php }} ?>
