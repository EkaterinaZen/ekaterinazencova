<?php /* Smarty version Smarty-3.1.21, created on 2015-11-30 13:11:09
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/staff/views/staff/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1593910585565b5743081904-10520653%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e2ead6a0bb92a74bfea71ca03782903a041f52b' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/staff/views/staff/update.tpl',
      1 => 1448878267,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1593910585565b5743081904-10520653',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565b57431152a2_27599775',
  'variables' => 
  array (
    'staff_data' => 0,
    'id' => 0,
    'selected_section' => 0,
    'users' => 0,
    'user' => 0,
    'runtime' => 0,
    'redirect_url' => 0,
    '_title' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565b57431152a2_27599775')) {function content_565b57431152a2_27599775($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('first_name','last_name','email','staff_function','user_id','images','text_staff_thumbnail','text_staff_detailed_image','new_staff','editing_staff','delete','create'));
?>
<?php if ($_smarty_tpl->tpl_vars['staff_data']->value) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['staff_data']->value['staff_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable(0, null, 0);?>
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("addons/staff/views/staff/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form name="staff_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit form-table" enctype="multipart/form-data">
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
    <input type="hidden" name="staff_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" class="cm-no-hide-input" name="selected_section" id="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_section']->value, ENT_QUOTES, 'UTF-8');?>
" />
        
    <div id="content_general"> 
        <div class="control-group cm-no-hide-input">
            <label for="first_name" class="control-label"><?php echo $_smarty_tpl->__("first_name");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[first_name]" id="first_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_data']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="last_name" class="control-label"><?php echo $_smarty_tpl->__("last_name");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[last_name]" id="last_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_data']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="email" class="control-label"><?php echo $_smarty_tpl->__("email");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[email]" id="email" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_data']->value['email'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="function" class="control-label"><?php echo $_smarty_tpl->__("staff_function");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[function]" id="last_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_data']->value['function'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>		
		<div class="control-group">
			<label class="control-label" for="user_id"><?php echo $_smarty_tpl->__("user_id");?>
</label>
			<div class="controls">
				<select class="span5" name="staff_data[user_id]" id="user_id">
				    <option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars["user"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["user"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["user"]->key => $_smarty_tpl->tpl_vars["user"]->value) {
$_smarty_tpl->tpl_vars["user"]->_loop = true;
?>
						<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['user']->value['user_id']==$_smarty_tpl->tpl_vars['staff_data']->value['user_id']) {?>selected="selected"<?php }?>><?php echo htmlspecialchars(fn_get_user_name($_smarty_tpl->tpl_vars['user']->value['user_id']), ENT_QUOTES, 'UTF-8');?>
</option>
					<?php } ?>
				</select>
			</div> 
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo $_smarty_tpl->__("images");?>
:</label>
			<div class="controls">
				<?php echo $_smarty_tpl->getSubTemplate ("common/attach_images.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_name'=>"staff_main",'image_object_type'=>"staff",'image_pair'=>$_smarty_tpl->tpl_vars['staff_data']->value['main_pair'],'icon_text'=>__("text_staff_thumbnail"),'detailed_text'=>__("text_staff_detailed_image"),'no_thumbnail'=>true), 0);?>

			</div>
		</div>		
		<?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"staff_data[status]",'id'=>"elm_staff_status",'obj'=>$_smarty_tpl->tpl_vars['staff_data']->value,'hidden'=>true), 0);?>

    </div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'group_name'=>$_smarty_tpl->tpl_vars['runtime']->value['controller'],'active_tab'=>$_smarty_tpl->tpl_vars['selected_section']->value,'track'=>true), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php ob_start();
echo $_smarty_tpl->__("new_staff");
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["_title"] = new Smarty_variable($_tmp1, null, 0);?>
<?php } else { ?>
    <?php ob_start();
echo $_smarty_tpl->__("editing_staff");
$_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["_title"] = new Smarty_variable($_tmp2.": ".((string)$_smarty_tpl->tpl_vars['staff_data']->value['first_name'])." ".((string)$_smarty_tpl->tpl_vars['staff_data']->value['last_name']), null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["redirect_url"] = new Smarty_variable("staff.manage", null, 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>__("delete"),'class'=>"cm-confirm cm-post",'href'=>"staff.delete?staff_id=".((string)$_smarty_tpl->tpl_vars['id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['redirect_url']->value)));?>
</li>     
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php if ($_smarty_tpl->tpl_vars['id']->value&&trim(Smarty::$_smarty_vars['capture']['tools_list'])!=='') {?>
        <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

    <?php }?>
    <div class="btn-group btn-hover dropleft">
        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_changes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"dropdown-toggle",'but_role'=>"submit-link",'but_name'=>"dispatch[staff.".((string)$_smarty_tpl->tpl_vars['runtime']->value['mode'])."]",'but_target_form'=>"staff_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>__("create"),'but_meta'=>"dropdown-toggle",'but_role'=>"submit-link",'but_name'=>"dispatch[staff.".((string)$_smarty_tpl->tpl_vars['runtime']->value['mode'])."]",'but_target_form'=>"staff_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

        <?php }?>
    </div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['_title']->value,'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons']), 0);?>

</form>
<?php }} ?>
