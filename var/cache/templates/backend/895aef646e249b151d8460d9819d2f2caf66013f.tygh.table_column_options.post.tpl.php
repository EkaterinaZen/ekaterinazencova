<?php /* Smarty version Smarty-3.1.21, created on 2015-11-30 13:44:10
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/product_picker/table_column_options.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:253377690565c287a257d70-18881761%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '895aef646e249b151d8460d9819d2f2caf66013f' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/product_picker/table_column_options.post.tpl',
      1 => 1441800576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '253377690565c287a257d70-18881761',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'extra_mode' => 0,
    'product_info' => 0,
    'item' => 0,
    'delete_id' => 0,
    'input_name' => 0,
    'clone' => 0,
    'ldelim' => 0,
    'rdelim' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_565c287a2d6b03_74218648',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565c287a2d6b03_74218648')) {function content_565c287a2d6b03_74218648($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('by_fixed','to_fixed','by_percentage','to_percentage','by_fixed','to_fixed','by_percentage','to_percentage'));
?>
<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=="buy_together"||$_smarty_tpl->tpl_vars['extra_mode']->value=="buy_together")&&$_smarty_tpl->tpl_vars['product_info']->value) {?>
    <td>
        <input type="hidden" id="item_price_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete_id']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product_info']->value['price'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product_info']->value['price']), 0);?>

    </td>
    <td>
        <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
[modifier_type]" class="input-slarge" id="item_modifier_type_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete_id']->value, ENT_QUOTES, 'UTF-8');?>
">
            <option value="by_fixed" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['modifier_type']=="by_fixed") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("by_fixed");?>
</option>
            <option value="to_fixed" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['modifier_type']=="to_fixed") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("to_fixed");?>
</option>
            <option value="by_percentage" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['modifier_type']=="by_percentage") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("by_percentage");?>
</option>
            <option value="to_percentage" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['modifier_type']=="to_percentage") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("to_percentage");?>
</option>
        </select>
    </td>
    <td>
        <input type="hidden" class="cm-chain-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
[modifier]" id="item_modifier_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete_id']->value, ENT_QUOTES, 'UTF-8');?>
" size="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product_info']->value['modifier'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="input-mini">
    </td>
    <td>
        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product_info']->value['discounted_price'],'span_id'=>"item_discounted_price_bt_".((string)$_smarty_tpl->tpl_vars['item']->value['chain_id'])."_".((string)$_smarty_tpl->tpl_vars['delete_id']->value)."_"), 0);?>

    </td>
    
<?php } elseif (($_smarty_tpl->tpl_vars['runtime']->value['controller']=="buy_together"||$_smarty_tpl->tpl_vars['extra_mode']->value=="buy_together")&&$_smarty_tpl->tpl_vars['clone']->value) {?>
    <td>
        <input type="text" class="hidden" id="item_price_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
bt_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
price<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
">
        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('span_id'=>"item_display_price_bt_".((string)$_smarty_tpl->tpl_vars['item']->value['chain_id'])."_".((string)$_smarty_tpl->tpl_vars['ldelim']->value)."bt_id".((string)$_smarty_tpl->tpl_vars['rdelim']->value)."_"), 0);?>

    </td>
    <td>
        <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
[modifier_type]" class="input-slarge" id="item_modifier_type_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
bt_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
">
            <option value="by_fixed"><?php echo $_smarty_tpl->__("by_fixed");?>
</option>
            <option value="to_fixed"><?php echo $_smarty_tpl->__("to_fixed");?>
</option>
            <option value="by_percentage"><?php echo $_smarty_tpl->__("by_percentage");?>
</option>
            <option value="to_percentage"><?php echo $_smarty_tpl->__("to_percentage");?>
</option>
        </select>
    </td>
    <td>
        <input type="text" class="cm-chain-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
 hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
bt_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="text" class="hidden" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
bt_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
[modifier]" id="item_modifier_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
bt_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
" size="4" value="0" class="input-mini">
    </td>
    <td>
        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('span_id'=>"item_discounted_price_bt_".((string)$_smarty_tpl->tpl_vars['item']->value['chain_id'])."_".((string)$_smarty_tpl->tpl_vars['ldelim']->value)."bt_id".((string)$_smarty_tpl->tpl_vars['rdelim']->value)."_"), 0);?>

    </td>
<?php }?><?php }} ?>
