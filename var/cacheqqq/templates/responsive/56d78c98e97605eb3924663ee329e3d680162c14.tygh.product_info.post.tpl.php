<?php /* Smarty version Smarty-3.1.21, created on 2015-11-05 17:01:31
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/checkout/product_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:213625114563b613b0dd865-87413393%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56d78c98e97605eb3924663ee329e3d680162c14' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/checkout/product_info.post.tpl',
      1 => 1446489755,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '213625114563b613b0dd865-87413393',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'key' => 0,
    'cart' => 0,
    'cart_products' => 0,
    'key_conf' => 0,
    '_product' => 0,
    'option' => 0,
    'file_id' => 0,
    'file' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563b613b20ff45_17373862',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563b613b20ff45_17373862')) {function content_563b613b20ff45_17373862($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('buy_together','buy_together','product','price','quantity','subtotal','buy_together','buy_together','product','price','quantity','subtotal'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key']->value]['extra']['buy_together']) {?>
    <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key_conf"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key_conf"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key_conf']->value]['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf_prod", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php }?>
    <?php } ?>

    <?php if (Smarty::$_smarty_vars['capture']['is_conf_prod']) {?>
        <h4><?php echo $_smarty_tpl->__("buy_together");?>
:</h4>
        <div class="ty-product-options" id="buy_together_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("buy_together");?>
">
            <table class="ty-table">
                <thead>
                <tr>
                    <th><?php echo $_smarty_tpl->__("product");?>
</th>
                    <th><?php echo $_smarty_tpl->__("price");?>
</th>
                    <th><?php echo $_smarty_tpl->__("quantity");?>
</th>
                    <th class="ty-right"><?php echo $_smarty_tpl->__("subtotal");?>
</th>
                </tr>
                </thead>
                <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key_conf"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key_conf"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key_conf']->value]['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
                        <tr>
                            <td>
                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['_product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"
                                   class="underlined"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['product'], ENT_QUOTES, 'UTF-8');?>
</a><br/>
                                <?php if ($_smarty_tpl->tpl_vars['_product']->value['product_options']) {?>
                                    <?php  $_smarty_tpl->tpl_vars["option"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["option"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_product']->value['product_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["option"]->key => $_smarty_tpl->tpl_vars["option"]->value) {
$_smarty_tpl->tpl_vars["option"]->_loop = true;
?>
                                        <strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['option_name'], ENT_QUOTES, 'UTF-8');?>
</strong>
                                        :&nbsp;
                                        <?php if ($_smarty_tpl->tpl_vars['option']->value['option_type']=="F") {?>
                                            <?php if ($_smarty_tpl->tpl_vars['_product']->value['extra']['custom_files'][$_smarty_tpl->tpl_vars['option']->value['option_id']]) {?>
                                                <?php  $_smarty_tpl->tpl_vars["file"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["file"]->_loop = false;
 $_smarty_tpl->tpl_vars["file_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_product']->value['extra']['custom_files'][$_smarty_tpl->tpl_vars['option']->value['option_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["file"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["file"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["file"]->key => $_smarty_tpl->tpl_vars["file"]->value) {
$_smarty_tpl->tpl_vars["file"]->_loop = true;
 $_smarty_tpl->tpl_vars["file_id"]->value = $_smarty_tpl->tpl_vars["file"]->key;
 $_smarty_tpl->tpl_vars["file"]->iteration++;
 $_smarty_tpl->tpl_vars["file"]->last = $_smarty_tpl->tpl_vars["file"]->iteration === $_smarty_tpl->tpl_vars["file"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["po_files"]['last'] = $_smarty_tpl->tpl_vars["file"]->last;
?>
                                                    <a class="cm-no-ajax"
                                                       href="<?php echo htmlspecialchars(fn_url("checkout.get_custom_file?cart_id=".((string)$_smarty_tpl->tpl_vars['key_conf']->value)."&file=".((string)$_smarty_tpl->tpl_vars['file_id']->value)."&option_id=".((string)$_smarty_tpl->tpl_vars['option']->value['option_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
                                                    <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['po_files']['last']) {?>,&nbsp;<?php }?>
                                                <?php } ?>
                                            <?php }?>
                                        <?php } else { ?>
                                            <?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['option']->value['variants'][$_smarty_tpl->tpl_vars['option']->value['value']]['variant_name'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['option']->value['value'] : $tmp), ENT_QUOTES, 'UTF-8');?>

                                        <?php }?>
                                        <input type="hidden" name="cart_products[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key_conf']->value, ENT_QUOTES, 'UTF-8');?>
][product_options][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['option_id'], ENT_QUOTES, 'UTF-8');?>
]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['value'], ENT_QUOTES, 'UTF-8');?>
">
                                        <br/>
                                    <?php } ?>
                                <?php }?>
                            </td>
                            <td>
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['price']), 0);?>
</td>
                            <td>
                                <input type="hidden" name="cart_products[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key_conf']->value, ENT_QUOTES, 'UTF-8');?>
][product_id]"
                                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
"/>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['amount'], ENT_QUOTES, 'UTF-8');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['display_subtotal']), 0);?>
</td>
                        </tr>
                    <?php }?>
                <?php } ?>
            </table>
        </div>
    <?php }?>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/buy_together/hooks/checkout/product_info.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/buy_together/hooks/checkout/product_info.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key']->value]['extra']['buy_together']) {?>
    <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key_conf"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key_conf"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key_conf']->value]['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf_prod", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php }?>
    <?php } ?>

    <?php if (Smarty::$_smarty_vars['capture']['is_conf_prod']) {?>
        <h4><?php echo $_smarty_tpl->__("buy_together");?>
:</h4>
        <div class="ty-product-options" id="buy_together_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("buy_together");?>
">
            <table class="ty-table">
                <thead>
                <tr>
                    <th><?php echo $_smarty_tpl->__("product");?>
</th>
                    <th><?php echo $_smarty_tpl->__("price");?>
</th>
                    <th><?php echo $_smarty_tpl->__("quantity");?>
</th>
                    <th class="ty-right"><?php echo $_smarty_tpl->__("subtotal");?>
</th>
                </tr>
                </thead>
                <?php  $_smarty_tpl->tpl_vars["_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key_conf"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_product"]->key => $_smarty_tpl->tpl_vars["_product"]->value) {
$_smarty_tpl->tpl_vars["_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key_conf"]->value = $_smarty_tpl->tpl_vars["_product"]->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['products'][$_smarty_tpl->tpl_vars['key_conf']->value]['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['key']->value) {?>
                        <tr>
                            <td>
                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['_product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"
                                   class="underlined"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['product'], ENT_QUOTES, 'UTF-8');?>
</a><br/>
                                <?php if ($_smarty_tpl->tpl_vars['_product']->value['product_options']) {?>
                                    <?php  $_smarty_tpl->tpl_vars["option"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["option"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_product']->value['product_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["option"]->key => $_smarty_tpl->tpl_vars["option"]->value) {
$_smarty_tpl->tpl_vars["option"]->_loop = true;
?>
                                        <strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['option_name'], ENT_QUOTES, 'UTF-8');?>
</strong>
                                        :&nbsp;
                                        <?php if ($_smarty_tpl->tpl_vars['option']->value['option_type']=="F") {?>
                                            <?php if ($_smarty_tpl->tpl_vars['_product']->value['extra']['custom_files'][$_smarty_tpl->tpl_vars['option']->value['option_id']]) {?>
                                                <?php  $_smarty_tpl->tpl_vars["file"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["file"]->_loop = false;
 $_smarty_tpl->tpl_vars["file_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_product']->value['extra']['custom_files'][$_smarty_tpl->tpl_vars['option']->value['option_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["file"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["file"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["file"]->key => $_smarty_tpl->tpl_vars["file"]->value) {
$_smarty_tpl->tpl_vars["file"]->_loop = true;
 $_smarty_tpl->tpl_vars["file_id"]->value = $_smarty_tpl->tpl_vars["file"]->key;
 $_smarty_tpl->tpl_vars["file"]->iteration++;
 $_smarty_tpl->tpl_vars["file"]->last = $_smarty_tpl->tpl_vars["file"]->iteration === $_smarty_tpl->tpl_vars["file"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["po_files"]['last'] = $_smarty_tpl->tpl_vars["file"]->last;
?>
                                                    <a class="cm-no-ajax"
                                                       href="<?php echo htmlspecialchars(fn_url("checkout.get_custom_file?cart_id=".((string)$_smarty_tpl->tpl_vars['key_conf']->value)."&file=".((string)$_smarty_tpl->tpl_vars['file_id']->value)."&option_id=".((string)$_smarty_tpl->tpl_vars['option']->value['option_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
                                                    <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['po_files']['last']) {?>,&nbsp;<?php }?>
                                                <?php } ?>
                                            <?php }?>
                                        <?php } else { ?>
                                            <?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['option']->value['variants'][$_smarty_tpl->tpl_vars['option']->value['value']]['variant_name'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['option']->value['value'] : $tmp), ENT_QUOTES, 'UTF-8');?>

                                        <?php }?>
                                        <input type="hidden" name="cart_products[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key_conf']->value, ENT_QUOTES, 'UTF-8');?>
][product_options][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['option_id'], ENT_QUOTES, 'UTF-8');?>
]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['value'], ENT_QUOTES, 'UTF-8');?>
">
                                        <br/>
                                    <?php } ?>
                                <?php }?>
                            </td>
                            <td>
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['price']), 0);?>
</td>
                            <td>
                                <input type="hidden" name="cart_products[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key_conf']->value, ENT_QUOTES, 'UTF-8');?>
][product_id]"
                                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
"/>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_product']->value['amount'], ENT_QUOTES, 'UTF-8');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['_product']->value['display_subtotal']), 0);?>
</td>
                        </tr>
                    <?php }?>
                <?php } ?>
            </table>
        </div>
    <?php }?>
<?php }?>
<?php }?><?php }} ?>
