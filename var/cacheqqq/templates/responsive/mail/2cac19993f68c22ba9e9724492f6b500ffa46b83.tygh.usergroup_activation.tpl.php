<?php /* Smarty version Smarty-3.1.21, created on 2015-11-05 15:50:15
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/profiles/usergroup_activation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1734550391563b5087b85350-75166654%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2cac19993f68c22ba9e9724492f6b500ffa46b83' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/profiles/usergroup_activation.tpl',
      1 => 1446457845,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1734550391563b5087b85350-75166654',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'usergroup_ids' => 0,
    'u_id' => 0,
    'usergroups' => 0,
    'user_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563b5087bca972_55798886',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563b5087bca972_55798886')) {function content_563b5087bca972_55798886($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('text_usergroup_activated','usergroups','usergroup','username','person_name'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("text_usergroup_activated");?>
<br>
<p>
<table>
<?php if ($_smarty_tpl->tpl_vars['usergroup_ids']->value) {?>
<tr>
    <td><?php if (sizeof($_smarty_tpl->tpl_vars['usergroup_ids']->value)>1) {
echo $_smarty_tpl->__("usergroups");
} else {
echo $_smarty_tpl->__("usergroup");
}?>:</td>
    <td>
        <?php  $_smarty_tpl->tpl_vars['u_id'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u_id']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['usergroup_ids']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['u_id']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['u_id']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['u_id']->key => $_smarty_tpl->tpl_vars['u_id']->value) {
$_smarty_tpl->tpl_vars['u_id']->_loop = true;
 $_smarty_tpl->tpl_vars['u_id']->iteration++;
 $_smarty_tpl->tpl_vars['u_id']->last = $_smarty_tpl->tpl_vars['u_id']->iteration === $_smarty_tpl->tpl_vars['u_id']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['ugroups']['last'] = $_smarty_tpl->tpl_vars['u_id']->last;
?>
            <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usergroups']->value[$_smarty_tpl->tpl_vars['u_id']->value]['usergroup'], ENT_QUOTES, 'UTF-8');?>
</b><?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['ugroups']['last']) {?>, <?php }?>
        <?php } ?>
    </td>
</tr>
<?php }?>
<tr>
    <td><?php echo $_smarty_tpl->__("username");?>
:</td>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
</tr>
<tr>
    <td><?php echo $_smarty_tpl->__("person_name");?>
:</td>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
</tr>
</table>
</p>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
