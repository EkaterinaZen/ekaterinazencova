<?php /* Smarty version Smarty-3.1.21, created on 2015-11-05 15:46:56
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/profiles/create_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1457190176563b4fc0abc144-82315481%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7f9579c7908e15a1289bf465b1d8b4b33bc44bb' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/profiles/create_profile.tpl',
      1 => 1446457845,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1457190176563b4fc0abc144-82315481',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user_data' => 0,
    'company_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563b4fc0aeab11_95643269',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563b4fc0aeab11_95643269')) {function content_563b4fc0aeab11_95643269($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/block.hook.php';
?><?php
fn_preload_lang_vars(array('dear','create_profile_notification_header'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("dear");?>
 <?php if ($_smarty_tpl->tpl_vars['user_data']->value['firstname']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(mb_strtolower(fn_get_user_type_description($_smarty_tpl->tpl_vars['user_data']->value['user_type']), 'UTF-8'), ENT_QUOTES, 'UTF-8');
}?>,<br><br>

<?php echo $_smarty_tpl->__("create_profile_notification_header");?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company_name'], ENT_QUOTES, 'UTF-8');?>
.<br><br>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"profiles:create_profile")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"profiles:create_profile"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"profiles:create_profile"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php echo $_smarty_tpl->getSubTemplate ("profiles/profiles_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('created'=>true), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
