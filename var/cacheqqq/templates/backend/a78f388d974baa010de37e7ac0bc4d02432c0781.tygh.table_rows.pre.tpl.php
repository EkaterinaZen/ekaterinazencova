<?php /* Smarty version Smarty-3.1.21, created on 2015-11-04 17:20:14
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/product_picker/table_rows.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1928907653563a141e107704-67538169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a78f388d974baa010de37e7ac0bc4d02432c0781' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/product_picker/table_rows.pre.tpl',
      1 => 1441800576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1928907653563a141e107704-67538169',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'extra_mode' => 0,
    'product_data' => 0,
    'item' => 0,
    'min_qty' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563a141e162fb8_22680207',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563a141e162fb8_22680207')) {function content_563a141e162fb8_22680207($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('by_fixed','to_fixed','by_percentage','to_percentage'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['controller']=="buy_together"||$_smarty_tpl->tpl_vars['extra_mode']->value=="buy_together") {?>

<?php if ($_smarty_tpl->tpl_vars['product_data']->value['min_qty']==0||$_smarty_tpl->tpl_vars['item']->value['min_qty']==0) {?>
    <?php $_smarty_tpl->tpl_vars["min_qty"] = new Smarty_variable("1", null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["min_qty"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['min_qty'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value['min_qty'] : $tmp), null, 0);?>
<?php }?>

<tr>
    <td><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['item']->value['product_name'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_data']->value['product'] : $tmp), ENT_QUOTES, 'UTF-8');?>
</td>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min_qty']->value, ENT_QUOTES, 'UTF-8');?>
</td>
    <td>
        <input type="hidden" id="item_price_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars((($tmp = @(($tmp = @$_smarty_tpl->tpl_vars['item']->value['price'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_data']->value['price'] : $tmp))===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="item_data_bt_[amount]" id="item_amount_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min_qty']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['price'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_data']->value['price'] : $tmp)), 0);?>

    </td>
    <td>
        <select id="item_modifier_type_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" class="input-slarge" name="item_data[modifier_type]">
            <option value="by_fixed" <?php if ($_smarty_tpl->tpl_vars['item']->value['modifier_type']=="by_fixed") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("by_fixed");?>
</option>
            <option value="to_fixed" <?php if ($_smarty_tpl->tpl_vars['item']->value['modifier_type']=="to_fixed") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("to_fixed");?>
</option>
            <option value="by_percentage" <?php if ($_smarty_tpl->tpl_vars['item']->value['modifier_type']=="by_percentage") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("by_percentage");?>
</option>
            <option value="to_percentage" <?php if ($_smarty_tpl->tpl_vars['item']->value['modifier_type']=="to_percentage") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("to_percentage");?>
</option>
        </select>
    </td>
    <td>
        <input type="hidden" class="cm-chain-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="text" name="item_data[modifier]" id="item_modifier_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['chain_id'], ENT_QUOTES, 'UTF-8');?>
" size="4" value="<?php echo htmlspecialchars(round((($tmp = @$_smarty_tpl->tpl_vars['item']->value['modifier'])===null||$tmp==='' ? 0 : $tmp),$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['decimals']), ENT_QUOTES, 'UTF-8');?>
" class="input-mini">
    </td>
    <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @(($tmp = @$_smarty_tpl->tpl_vars['item']->value['discounted_price'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_data']->value['price'] : $tmp))===null||$tmp==='' ? "0" : $tmp),'span_id'=>"item_discounted_price_bt_".((string)$_smarty_tpl->tpl_vars['item']->value['chain_id'])."_".((string)$_smarty_tpl->tpl_vars['item']->value['chain_id'])."_"), 0);?>
</td>
    <td>&nbsp;</td>
</tr>
<?php }?><?php }} ?>
