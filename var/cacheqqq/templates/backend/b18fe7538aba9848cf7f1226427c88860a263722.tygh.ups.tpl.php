<?php /* Smarty version Smarty-3.1.21, created on 2015-11-02 15:32:44
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/views/shippings/components/services/ups.tpl" */ ?>
<?php /*%%SmartyHeaderCode:127295763563757ec711b51-98597197%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b18fe7538aba9848cf7f1226427c88860a263722' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/views/shippings/components/services/ups.tpl',
      1 => 1441800579,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '127295763563757ec711b51-98597197',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shipping' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563757ec7da380_85329238',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563757ec7da380_85329238')) {function content_563757ec7da380_85329238($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('ship_ups_access_key','username','password','use_negotiated_rates','shipper_number','test_mode','ship_ups_pickup_type','ship_ups_pickup_type_01','ship_ups_pickup_type_03','ship_ups_pickup_type_06','ship_ups_pickup_type_07','ship_ups_pickup_type_11','ship_ups_pickup_type_19','ship_ups_pickup_type_20','package_type','ship_ups_package_type_01','package','ship_ups_package_type_03','ship_ups_package_type_04','ship_ups_package_type_21','ship_ups_package_type_24','ship_ups_package_type_25','ship_ups_use_delivery_confirmation','ship_ups_dcist_type','ship_ups_delivery_confirmation','ship_ups_delivery_confirmation_signature','ship_ups_delivery_confirmation_adult_signature','max_box_weight','width','height','length'));
?>
<fieldset>

<div class="control-group">
    <label class="control-label" for="ship_ups_access_key"><?php echo $_smarty_tpl->__("ship_ups_access_key");?>
</label>
    <div class="controls">
    <input id="ship_ups_access_key" type="text" name="shipping_data[service_params][access_key]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['access_key'], ENT_QUOTES, 'UTF-8');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="username"><?php echo $_smarty_tpl->__("username");?>
</label>
    <div class="controls">
    <input id="username" type="text" name="shipping_data[service_params][username]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['username'], ENT_QUOTES, 'UTF-8');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="password"><?php echo $_smarty_tpl->__("password");?>
</label>
    <div class="controls">
    <input id="password" type="text" name="shipping_data[service_params][password]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['password'], ENT_QUOTES, 'UTF-8');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="sw_negotiated_rates"><?php echo $_smarty_tpl->__("use_negotiated_rates");?>
</label>
    <div class="controls">
    <input type="hidden" name="shipping_data[service_params][negotiated_rates]" value="N" />
    <input id="sw_negotiated_rates" type="checkbox" name="shipping_data[service_params][negotiated_rates]" value="Y" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['negotiated_rates']=="Y") {?>checked="checked"<?php }?> class="cm-combination" />
    </div>
</div>

<div id="negotiated_rates" class="<?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['negotiated_rates']!="Y") {?>hidden<?php }?>">
    <div class="control-group">
        <label class="control-label" for="shipper_number"><?php echo $_smarty_tpl->__("shipper_number");?>
</label>
        <div class="controls">
        <input id="shipper_number" type="text" name="shipping_data[service_params][shipper_number]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['shipper_number'], ENT_QUOTES, 'UTF-8');?>
"/>
        </div>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="test_mode"><?php echo $_smarty_tpl->__("test_mode");?>
</label>
    <div class="controls">
    <input type="hidden" name="shipping_data[service_params][test_mode]" value="N" />
    <input id="test_mode" type="checkbox" name="shipping_data[service_params][test_mode]" value="Y" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['test_mode']=="Y") {?>checked="checked"<?php }?> />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_ups_pickup_type"><?php echo $_smarty_tpl->__("ship_ups_pickup_type");?>
</label>
    <div class="controls">
    <select id="ship_ups_pickup_type" name="shipping_data[service_params][pickup_type]">
        <option value="01" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="01") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_01");?>
</option>
        <option value="03" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="03") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_03");?>
</option>
        <option value="06" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="06") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_06");?>
</option>
        <option value="07" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="07") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_07");?>
</option>
        <option value="11" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="11") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_11");?>
</option>
        <option value="19" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="19") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_19");?>
</option>
        <option value="20" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['pickup_type']=="20") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_pickup_type_20");?>
</option>
    </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="package_type"><?php echo $_smarty_tpl->__("package_type");?>
</label>
    <div class="controls">
    <select id="package_type" name="shipping_data[service_params][package_type]">
        <option value="01" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="01") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_01");?>
</option>
        <option value="02" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="02") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("package");?>
</option>
        <option value="03" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="03") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_03");?>
</option>
        <option value="04" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="04") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_04");?>
</option>
        <option value="21" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="21") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_21");?>
</option>
        <option value="24" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="24") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_24");?>
</option>
        <option value="25" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['package_type']=="25") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_package_type_25");?>
</option>
    </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="sw_delivery_confirmation"><?php echo $_smarty_tpl->__("ship_ups_use_delivery_confirmation");?>
</label>
    <div class="controls">
    <input type="hidden" name="shipping_data[service_params][delivery_confirmation]" value="N" />
    <input id="sw_delivery_confirmation" type="checkbox" name="shipping_data[service_params][delivery_confirmation]" value="Y" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['delivery_confirmation']=="Y") {?>checked="checked"<?php }?> class="cm-combination" />
    </div>
</div>

<div id="delivery_confirmation" class="control-group <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['delivery_confirmation']!="Y") {?>hidden<?php }?>">
    <label class="control-label" for="dcist_type"><?php echo $_smarty_tpl->__("ship_ups_dcist_type");?>
</label>
    <div class="controls">
    <select id="dcist_type" name="shipping_data[service_params][dcist_type]">
        <option value="1" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['dcist_type']=="1") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_delivery_confirmation");?>
</option>
        <option value="2" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['dcist_type']=="2") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_delivery_confirmation_signature");?>
</option>
        <option value="3" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['dcist_type']=="3") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("ship_ups_delivery_confirmation_adult_signature");?>
</option>
    </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="max_weight"><?php echo $_smarty_tpl->__("max_box_weight");?>
</label>
    <div class="controls">
    <input id="max_weight" type="text" name="shipping_data[service_params][max_weight_of_box]" size="30" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['shipping']->value['service_params']['max_weight_of_box'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="width"><?php echo $_smarty_tpl->__("width");?>
</label>
    <div class="controls">
    <input id="width" type="text" name="shipping_data[service_params][width]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['width'], ENT_QUOTES, 'UTF-8');?>
"/></div>
</div>

<div class="control-group">
    <label class="control-label" for="height"><?php echo $_smarty_tpl->__("height");?>
</label>
    <div class="controls">
    <input id="height" type="text" name="shipping_data[service_params][height]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['height'], ENT_QUOTES, 'UTF-8');?>
" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="length"><?php echo $_smarty_tpl->__("length");?>
</label>
    <div class="controls">
    <input id="length" type="text" name="shipping_data[service_params][length]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['length'], ENT_QUOTES, 'UTF-8');?>
"/>
    </div>
</div>

</fieldset><?php }} ?>
