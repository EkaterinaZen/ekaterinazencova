<?php /* Smarty version Smarty-3.1.21, created on 2015-11-04 17:20:13
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/views/buy_together/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1004059166563a141dea2455-49946883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37cf895bbacd24880a35d70afdf3c09f4dc8fed6' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/views/buy_together/update.tpl',
      1 => 1441800576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1004059166563a141dea2455-49946883',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'runtime' => 0,
    'allow_save' => 0,
    'product_id' => 0,
    'id' => 0,
    'no_hide_inputs' => 0,
    'settings' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
    'hide_first_button' => 0,
    'hide_second_button' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563a141e059282_10385802',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563a141e059282_10385802')) {function content_563a141e059282_10385802($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('general','products','name','description','avail_from','avail_till','display_in_promotions','combination_products','recalculate','total_cost','price_for_all','share_discount','apply'));
?>
<?php if ($_smarty_tpl->tpl_vars['item']->value['chain_id']) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['chain_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["extra_mode"] = new Smarty_variable("buy_together", null, 0);?>
<?php }?>

<?php if (fn_allow_save_object($_smarty_tpl->tpl_vars['item']->value,"chains")&&($_smarty_tpl->tpl_vars['runtime']->value['company_id']||$_smarty_tpl->tpl_vars['runtime']->value['simple_ultimate'])) {?>
    <?php $_smarty_tpl->tpl_vars["allow_save"] = new Smarty_variable(true, null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["allow_save"] = new Smarty_variable(false, null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['allow_save']->value) {?>
    <?php $_smarty_tpl->tpl_vars["no_hide_inputs"] = new Smarty_variable(" cm-no-hide-input", null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['item']->value['product_id']) {?>
    <?php $_smarty_tpl->tpl_vars["product_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['product_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["product_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product_id']->value, null, 0);?>
<?php }?>

<div id="content_group_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="item_update_form_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="<?php if (!$_smarty_tpl->tpl_vars['allow_save']->value) {?> cm-hide-inputs<?php }?> form-horizontal form-edit" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="item_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" class="cm-no-hide-input" name="product_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_id']->value, ENT_QUOTES, 'UTF-8');?>
" />

<div class="tabs cm-j-tabs">
    <ul class="nav nav-tabs">
        <li id="tab_general_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-js active"><a><?php echo $_smarty_tpl->__("general");?>
</a></li>
        <li id="tab_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-js"><a><?php echo $_smarty_tpl->__("products");?>
</a></li>
    </ul>
</div>

<div class="cm-tabs-content" id="tabs_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
    <fieldset>
        <div id="content_tab_general_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label for="elm_buy_together_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="control-label cm-required"><?php echo $_smarty_tpl->__("name");?>
:</label>
                <div class="controls">
                    <input type="text" name="item_data[name]" id="elm_buy_together_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8');?>
" class="span9">
                </div>
            </div>
            
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label class="control-label" for="elm_buy_together_description_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("description");?>
:</label>
                <div class="controls">
                        <textarea id="elm_buy_together_description_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="item_data[description]" cols="55" rows="8" class="cm-wysiwyg input-textarea-long"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
                </div>
            </div>
            
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label class="control-label" for="elm_buy_together_avail_from_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("avail_from");?>
:</label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_buy_together_avail_from_".((string)$_smarty_tpl->tpl_vars['id']->value),'date_name'=>"item_data[date_from]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['date_from'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

                </div>
            </div>
            
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label class="control-label" for="elm_buy_together_avail_till_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("avail_till");?>
:</label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_buy_together_avail_till_".((string)$_smarty_tpl->tpl_vars['id']->value),'date_name'=>"item_data[date_to]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['date_to'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

                </div>
            </div>
            
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label class="control-label" for="elm_buy_together_promotions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("display_in_promotions");?>
:</label>
               <div class="controls">
                    <input type="hidden" name="item_data[display_in_promotions]" value="N">
                    <input type="checkbox" name="item_data[display_in_promotions]" id="elm_buy_together_promotions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" value="Y" <?php if ($_smarty_tpl->tpl_vars['item']->value['display_in_promotions']=="Y") {?>checked="checked"<?php }?>>
                </div>
            </div>
            
            <?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"item_data[status]",'id'=>"elm_buy_together_status_".((string)$_smarty_tpl->tpl_vars['id']->value),'obj'=>$_smarty_tpl->tpl_vars['item']->value,'hidden'=>false), 0);?>

        </div>
        
        <div id="content_tab_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['no_hide_inputs']->value) {?>class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("combination_products")), 0);?>

            
            <?php echo $_smarty_tpl->getSubTemplate ("pickers/products/picker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('data_id'=>"objects_".((string)$_smarty_tpl->tpl_vars['id']->value)."_",'input_name'=>"item_data[products]",'item_ids'=>$_smarty_tpl->tpl_vars['item']->value['products_info'],'type'=>"table",'aoc'=>true,'colspan'=>"7",'placement'=>"right"), 0);?>

            
            <ul class="pull-right unstyled right span6">
            <?php if ($_smarty_tpl->tpl_vars['allow_save']->value) {?>
                <li>
                    <a class="btn" onclick="fn_buy_together_recalculate('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
');"><?php echo $_smarty_tpl->__("recalculate");?>
</a><br><br>
                </li>
            <?php }?>
                <li>
                    <em><?php echo $_smarty_tpl->__("total_cost");?>
:</em>
                    <strong><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['item']->value['total_price'],'span_id'=>"total_price_".((string)$_smarty_tpl->tpl_vars['id']->value)), 0);?>
</strong>
                </li>
                <li>
                    <em><?php echo $_smarty_tpl->__("price_for_all");?>
:</em>
                    <strong><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['item']->value['chain_price'],'span_id'=>"price_for_all_".((string)$_smarty_tpl->tpl_vars['id']->value)), 0);?>
</strong>
                </li>
            <?php if ($_smarty_tpl->tpl_vars['allow_save']->value) {?>
                <li><br>
                    <label for="elm_buy_together_global_discount_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><em><?php echo $_smarty_tpl->__("share_discount");?>
&nbsp;(<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</em>&nbsp;<input type="text" class="input-mini" size="4" id="elm_buy_together_global_discount_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" onkeypress="fn_buy_together_share_discount(event, '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
');" />&nbsp;<a onclick="fn_buy_together_apply_discount('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
');" class="btn"><?php echo $_smarty_tpl->__("apply");?>
</a></label>
                </li>
            <?php }?>
            </ul>            
        </div>
    </fieldset>
</div>

<div class="buttons-container">    
    <?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[buy_together.update]",'cancel_action'=>"close"), 0);?>

    <?php } else { ?>
        <?php if (fn_allowed_for("MULTIVENDOR")) {?>
            <?php if (!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
                <?php $_smarty_tpl->tpl_vars["hide_first_button"] = new Smarty_variable(true, null, 0);?>
            <?php }?>
        <?php }?>

        <?php if (fn_allowed_for("ULTIMATE")&&!$_smarty_tpl->tpl_vars['allow_save']->value) {?>
            <?php $_smarty_tpl->tpl_vars["hide_first_button"] = new Smarty_variable(true, null, 0);?>
            <?php $_smarty_tpl->tpl_vars["hide_second_button"] = new Smarty_variable(true, null, 0);?>
        <?php }?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[buy_together.update]",'cancel_action'=>"close",'hide_first_button'=>$_smarty_tpl->tpl_vars['hide_first_button']->value,'hide_second_button'=>$_smarty_tpl->tpl_vars['hide_second_button']->value,'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

    <?php }?>
</div>

</form>

<!--content_group_bt_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
--></div><?php }} ?>
