<?php /* Smarty version Smarty-3.1.21, created on 2015-11-06 16:03:21
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/views/products/components/file_tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1219804067563ca5195434c3-30752803%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56261c8f2b2312f647ff4cd125701514a2b0daa0' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/views/products/components/file_tree.tpl',
      1 => 1441800579,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1219804067563ca5195434c3-30752803',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'level' => 0,
    'product_file' => 0,
    'shift' => 0,
    'non_editable' => 0,
    'id_prefix' => 0,
    'id' => 0,
    'tool_items' => 0,
    'skip_delete' => 0,
    'product_data' => 0,
    'hidden' => 0,
    'hide_for_vendor' => 0,
    'display' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_563ca5195af325_83636112',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_563ca5195af325_83636112')) {function content_563ca5195af325_83636112($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/ezencova/public_html/cscart/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
?><?php
fn_preload_lang_vars(array('editing_file','delete'));
?>
<?php echo smarty_function_math(array('equation'=>"x*30+5",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['level']->value)===null||$tmp==='' ? "0" : $tmp),'assign'=>"shift"),$_smarty_tpl);?>

<tr class="multiple-table-row cm-row-status-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['product_file']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
 ">
    <td>
        <div style="padding-left: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shift']->value, ENT_QUOTES, 'UTF-8');?>
px;">
            <a class="row-status cm-external-click<?php if ($_smarty_tpl->tpl_vars['non_editable']->value) {?> no-underline<?php }?>" data-ca-external-click-id="opener_group<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_file']->value['file_name'], ENT_QUOTES, 'UTF-8');?>
</a>
        </div></td>

    <td width="10%" class="right nowrap">
        <div class="pull-right hidden-tools">
            <?php $_smarty_tpl->_capture_stack[0][] = array("items_tools", null, null); ob_start(); ?>
                <?php if ($_smarty_tpl->tpl_vars['tool_items']->value) {?>
                    <?php echo $_smarty_tpl->tpl_vars['tool_items']->value;?>

                <?php }?>

                <?php if (!$_smarty_tpl->tpl_vars['non_editable']->value) {?>
                    <li><?php ob_start();
echo $_smarty_tpl->__("editing_file");
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"group".((string)$_smarty_tpl->tpl_vars['id_prefix']->value).((string)$_smarty_tpl->tpl_vars['id']->value),'text'=>$_tmp1.": ".((string)$_smarty_tpl->tpl_vars['product_file']->value['file_name']),'act'=>"edit",'opener_ajax_class'=>"cm-ajax"), 0);?>
</li>
                <?php }?>

                <?php if (!$_smarty_tpl->tpl_vars['non_editable']->value&&!$_smarty_tpl->tpl_vars['skip_delete']->value) {?>
                    <li><?php ob_start();?><?php echo htmlspecialchars(fn_url("products.delete_file?file_id=".((string)$_smarty_tpl->tpl_vars['product_file']->value['file_id'])."&product_id=".((string)$_smarty_tpl->tpl_vars['product_data']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
<?php $_tmp2=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"text",'text'=>__("delete"),'href'=>$_tmp2,'class'=>"cm-confirm cm-tooltip cm-ajax cm-post cm-ajax-force cm-ajax-full-render cm-delete-row",'data'=>array("data-ca-target-id"=>"product_files_list")));?>
</li>
                <?php }?>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['items_tools']));?>

        </div></td>

    <td width="15%">
        <div class="pull-right nowrap">
            <?php if ($_smarty_tpl->tpl_vars['non_editable']->value==true) {?>
                <?php $_smarty_tpl->tpl_vars["display"] = new Smarty_variable("text", null, 0);?>
            <?php }?>
            
            <?php echo $_smarty_tpl->getSubTemplate ("common/select_popup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('popup_additional_class'=>"dropleft",'id'=>$_smarty_tpl->tpl_vars['id']->value,'status'=>$_smarty_tpl->tpl_vars['product_file']->value['status'],'hidden'=>$_smarty_tpl->tpl_vars['hidden']->value,'object_id_name'=>"file_id",'table'=>"product_files",'hide_for_vendor'=>$_smarty_tpl->tpl_vars['hide_for_vendor']->value,'display'=>$_smarty_tpl->tpl_vars['display']->value,'non_editable'=>$_smarty_tpl->tpl_vars['non_editable']->value), 0);?>

        </div></td>
</tr><?php }} ?>
