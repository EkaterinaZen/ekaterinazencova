<?php /* Smarty version Smarty-3.1.21, created on 2015-11-02 15:50:08
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/views/destinations/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:99539164856375c008f41a0-02248478%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '342d99a1aa2c30c0a36851da2a6f9b9353e52d61' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/views/destinations/update.tpl',
      1 => 1441800579,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '99539164856375c008f41a0-02248478',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'destination' => 0,
    'id' => 0,
    'destination_data' => 0,
    'countries' => 0,
    'states' => 0,
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56375c0096f9c1_14259037',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56375c0096f9c1_14259037')) {function content_56375c0096f9c1_14259037($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('general','name','countries','states','zipcodes','text_zipcodes_wildcards','cities','text_cities_wildcards','addresses','text_addresses_wildcards','new_location','editing_location'));
?>
<?php if ($_smarty_tpl->tpl_vars['destination']->value) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['destination']->value['destination_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable(0, null, 0);?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("general")), 0);?>


<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="destinations_form" class="form-horizontal form-edit <?php if (fn_check_form_permissions('')) {?> cm-hide-inputs<?php }?>">
<input type="hidden" name="destination_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />

<div class="control-group">
    <label for="elm_destination_name" class="control-label cm-required"><?php echo $_smarty_tpl->__("name");?>
:</label>
    <div class="controls">
        <input type="text" name="destination_data[destination]" id="elm_destination_name" size="25" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['destination']->value['destination'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
    </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("views/localizations/components/select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('data_name'=>"destination_data[localization]",'data_from'=>$_smarty_tpl->tpl_vars['destination']->value['localization']), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"destination_data[status]",'id'=>"elm_destination_status",'obj'=>$_smarty_tpl->tpl_vars['destination']->value), 0);?>


<hr />



<?php echo $_smarty_tpl->getSubTemplate ("common/double_selectboxes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("countries"),'first_name'=>"destination_data[countries]",'first_data'=>$_smarty_tpl->tpl_vars['destination_data']->value['countries'],'second_name'=>"all_countries",'second_data'=>$_smarty_tpl->tpl_vars['countries']->value,'class_name'=>"destination-countries"), 0);?>

<hr />



<?php echo $_smarty_tpl->getSubTemplate ("common/double_selectboxes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("states"),'first_name'=>"destination_data[states]",'first_data'=>$_smarty_tpl->tpl_vars['destination_data']->value['states'],'second_name'=>"all_states",'second_data'=>$_smarty_tpl->tpl_vars['states']->value,'class_name'=>"destination-states"), 0);?>

<hr />


<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("zipcodes")), 0);?>

<table width="100%">
<tr>
    <td width="48%">
        <textarea name="destination_data[zipcodes]" id="elm_destination_zipcodes" rows="8" class="input-full"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['destination_data']->value['zipcodes'], ENT_QUOTES, 'UTF-8');?>
</textarea></td>
    <td>&nbsp;</td>

    <td width="48%"><?php echo $_smarty_tpl->__("text_zipcodes_wildcards");?>
</td>
</tr>
</table>

<hr />


<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("cities")), 0);?>

<table cellpadding="0" cellspacing="0" width="100%"    border="0">
<tr>
    <td width="48%">
        <textarea name="destination_data[cities]" id="elm_destination_cities" rows="8" class="input-full"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['destination_data']->value['cities'], ENT_QUOTES, 'UTF-8');?>
</textarea></td>
    <td>&nbsp;</td>

    <td width="48%"><?php echo $_smarty_tpl->__("text_cities_wildcards");?>
</td>
</tr>
</table>

<hr />


<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("addresses")), 0);?>

<table cellpadding="0" cellspacing="0" width="100%"    border="0">
<tr>
    <td width="48%">
        <textarea name="destination_data[addresses]" id="elm_destination_cities" rows="8" class="input-full"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['destination_data']->value['addresses'], ENT_QUOTES, 'UTF-8');?>
</textarea></td>
    <td>&nbsp;</td>

    <td width="48%"><?php echo $_smarty_tpl->__("text_addresses_wildcards");?>
</td>
</tr>
</table>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[destinations.update]",'but_target_form'=>"destinations_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_smarty_tpl->__("new_location"), null, 0);?>
<?php } else { ?>
    <?php ob_start();
echo $_smarty_tpl->__("editing_location");
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_tmp1.": ".((string)$_smarty_tpl->tpl_vars['destination']->value['destination']), null, 0);?>
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['title']->value,'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
