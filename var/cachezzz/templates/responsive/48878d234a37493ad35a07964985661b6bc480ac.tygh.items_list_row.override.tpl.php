<?php /* Smarty version Smarty-3.1.21, created on 2015-11-09 10:25:22
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:64021996456404a62043a10-47453628%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '48878d234a37493ad35a07964985661b6bc480ac' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl',
      1 => 1446489755,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '64021996456404a62043a10-47453628',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'product' => 0,
    'order_info' => 0,
    'sub_oi' => 0,
    'conf_price' => 0,
    'conf_discount' => 0,
    'conf_tax' => 0,
    'conf_subtotal' => 0,
    'c_product' => 0,
    'settings' => 0,
    '_colspan' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56404a622528b0_78085300',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56404a622528b0_78085300')) {function content_56404a622528b0_78085300($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/ezencova/public_html/cscart/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_block_hook')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('download','sku','buy_together','sku','price','quantity','discount','tax','subtotal','download','sku','buy_together','sku','price','quantity','discount','tax','subtotal'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['product']->value['extra']['buy_together']) {?>
    <?php $_smarty_tpl->tpl_vars["conf_price"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['price'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_subtotal"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_discount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_tax"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>

    <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable(4, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["c_product"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars["sub_oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sub_oi"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sub_oi"]->key => $_smarty_tpl->tpl_vars["sub_oi"]->value) {
$_smarty_tpl->tpl_vars["sub_oi"]->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['product']->value['cart_id']) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_function_math(array('equation'=>"item_price * amount + conf_price",'amount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['min_qty'])===null||$tmp==='' ? "1" : $tmp),'item_price'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['price'])===null||$tmp==='' ? "0" : $tmp),'conf_price'=>$_smarty_tpl->tpl_vars['conf_price']->value,'assign'=>"conf_price"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"discount + conf_discount",'discount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp),'conf_discount'=>$_smarty_tpl->tpl_vars['conf_discount']->value,'assign'=>"conf_discount"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"tax + conf_tax",'tax'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp),'conf_tax'=>$_smarty_tpl->tpl_vars['conf_tax']->value,'assign'=>"conf_tax"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"subtotal + conf_subtotal",'subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp),'conf_subtotal'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value,'assign'=>"conf_subtotal"),$_smarty_tpl);?>

        <?php }?>
    <?php } ?>

    <tr class="ty-buy-together-orders ty-valign-top">
        <td class="ty-valign-top">
            <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
                <?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>

            <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>

            <?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['is_edp']=="Y") {?>
                <div class="ty-right"><a href="<?php echo htmlspecialchars(fn_url("orders.order_downloads?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">[<?php echo $_smarty_tpl->__("download");?>
]</a></div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
            <?php }?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                <?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {
echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options']), 0);
}?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            
            <?php if (Smarty::$_smarty_vars['capture']['is_conf']) {?>
                <div><strong class="combination-link ty-hand"><?php echo $_smarty_tpl->__("buy_together");?>
</strong></div>

                <div class="ty-buy-together-orders__products" >
                    <div class="ty-cart-content-products">
                    <div class="ty-caret-info"><span class="ty-caret-outer"></span><span class="ty-caret-inner"></span></div>
                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["sub_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["sub_key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['product']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['c_product']->value['cart_id']) {?>
                            <div class="ty-cart-content-products__item">
                                <div>
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }
echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],50,"...",true);
if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>&nbsp;
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                                    <p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p>
                                    <?php }?>
                                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options']), 0);?>

                                    <?php }?>
                                    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                    </span>
                                </div>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("price");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price']), 0);?>

                                    </span>
                                </div>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("quantity");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>

                                    </span>
                                </div>
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("discount");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discount']), 0);
} else { ?>-<?php }?>
                                    </span>
                                </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("tax");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_value']), 0);?>

                                    </span>
                                </div>
                                <?php }?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("subtotal");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['display_subtotal']), 0);?>

                                    </span>
                                </div>
                            </div>
                        <?php }?>
                        <?php } ?>
                    </div>
                </div>

            <?php }?>
        </td>
        <td class="ty-right">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_price']->value), 0);?>

        </td>
        <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
            <td class="ty-right">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_discount']->value), 0);?>

            </td>
            <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
            <td class="ty-center">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_tax']->value), 0);?>

            </td>
        <?php }?>
        <td class="ty-right">
            &nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value), 0);?>

        </td>
    </tr>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/buy_together/hooks/orders/items_list_row.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/buy_together/hooks/orders/items_list_row.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['product']->value['extra']['buy_together']) {?>
    <?php $_smarty_tpl->tpl_vars["conf_price"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['price'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_subtotal"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_discount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_tax"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>

    <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable(4, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["c_product"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars["sub_oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sub_oi"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sub_oi"]->key => $_smarty_tpl->tpl_vars["sub_oi"]->value) {
$_smarty_tpl->tpl_vars["sub_oi"]->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['product']->value['cart_id']) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_function_math(array('equation'=>"item_price * amount + conf_price",'amount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['min_qty'])===null||$tmp==='' ? "1" : $tmp),'item_price'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['price'])===null||$tmp==='' ? "0" : $tmp),'conf_price'=>$_smarty_tpl->tpl_vars['conf_price']->value,'assign'=>"conf_price"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"discount + conf_discount",'discount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp),'conf_discount'=>$_smarty_tpl->tpl_vars['conf_discount']->value,'assign'=>"conf_discount"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"tax + conf_tax",'tax'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp),'conf_tax'=>$_smarty_tpl->tpl_vars['conf_tax']->value,'assign'=>"conf_tax"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"subtotal + conf_subtotal",'subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp),'conf_subtotal'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value,'assign'=>"conf_subtotal"),$_smarty_tpl);?>

        <?php }?>
    <?php } ?>

    <tr class="ty-buy-together-orders ty-valign-top">
        <td class="ty-valign-top">
            <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
                <?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>

            <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>

            <?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['is_edp']=="Y") {?>
                <div class="ty-right"><a href="<?php echo htmlspecialchars(fn_url("orders.order_downloads?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">[<?php echo $_smarty_tpl->__("download");?>
]</a></div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
            <?php }?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                <?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {
echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options']), 0);
}?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            
            <?php if (Smarty::$_smarty_vars['capture']['is_conf']) {?>
                <div><strong class="combination-link ty-hand"><?php echo $_smarty_tpl->__("buy_together");?>
</strong></div>

                <div class="ty-buy-together-orders__products" >
                    <div class="ty-cart-content-products">
                    <div class="ty-caret-info"><span class="ty-caret-outer"></span><span class="ty-caret-inner"></span></div>
                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["sub_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["sub_key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['product']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['c_product']->value['cart_id']) {?>
                            <div class="ty-cart-content-products__item">
                                <div>
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }
echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],50,"...",true);
if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>&nbsp;
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                                    <p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p>
                                    <?php }?>
                                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options']), 0);?>

                                    <?php }?>
                                    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                    </span>
                                </div>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("price");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price']), 0);?>

                                    </span>
                                </div>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("quantity");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>

                                    </span>
                                </div>
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("discount");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discount']), 0);
} else { ?>-<?php }?>
                                    </span>
                                </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("tax");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_value']), 0);?>

                                    </span>
                                </div>
                                <?php }?>
                                <div class="ty-control-group">
                                    <strong class="ty-control-group__label"><?php echo $_smarty_tpl->__("subtotal");?>
</strong>
                                    <span class="ty-control-group__item">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['display_subtotal']), 0);?>

                                    </span>
                                </div>
                            </div>
                        <?php }?>
                        <?php } ?>
                    </div>
                </div>

            <?php }?>
        </td>
        <td class="ty-right">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_price']->value), 0);?>

        </td>
        <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
            <td class="ty-right">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_discount']->value), 0);?>

            </td>
            <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
            <td class="ty-center">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_tax']->value), 0);?>

            </td>
        <?php }?>
        <td class="ty-right">
            &nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value), 0);?>

        </td>
    </tr>
<?php }
}?><?php }} ?>
