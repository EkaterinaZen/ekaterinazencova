<?php /* Smarty version Smarty-3.1.21, created on 2015-11-15 13:23:59
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/order_period/hooks/products/buttons_block.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:106538164356403ecb9df4b9-53881359%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a2a24db0c86e2c7374ca7a33e0dcf59b3bc92ff' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/order_period/hooks/products/buttons_block.pre.tpl',
      1 => 1447580576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '106538164356403ecb9df4b9-53881359',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56403ecba2b291_59795261',
  'variables' => 
  array (
    'runtime' => 0,
    'obj_id' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56403ecba2b291_59795261')) {function content_56403ecba2b291_59795261($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('select_type','by_period','by_date','select_period','month','months','months','select_date','select_type','by_period','by_date','select_period','month','months','months','select_date'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ty-control-group product-list-field">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_type");?>
</label>
    <span class="ty-control-group__item">
        <select id="change_period" name="product_data[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
][extra][period_options][by_type]">
            <option value="by_period"><?php echo $_smarty_tpl->__("by_period");?>
</option>
            <option value="by_date"><?php echo $_smarty_tpl->__("by_date");?>
</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field" id="by_period">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_period");?>
</label>
    <span class="ty-control-group__item">
        <select name="product_data[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
][extra][period_options][by_period]">
            <option>1 <?php echo $_smarty_tpl->__("month");?>
</option>
            <option>3 <?php echo $_smarty_tpl->__("months");?>
</option>
            <option>6 <?php echo $_smarty_tpl->__("months");?>
</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field hidden" id="by_date">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_date");?>
</label>
    <span class="ty-control-group__item">
        <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_order_date",'date_name'=>"product_data[".((string)$_smarty_tpl->tpl_vars['obj_id']->value)."][extra][period_options][by_date]",'date_val'=>date("Y-m-d")), 0);?>

    </span>
</div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/order_period/hooks/products/buttons_block.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/order_period/hooks/products/buttons_block.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ty-control-group product-list-field">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_type");?>
</label>
    <span class="ty-control-group__item">
        <select id="change_period" name="product_data[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
][extra][period_options][by_type]">
            <option value="by_period"><?php echo $_smarty_tpl->__("by_period");?>
</option>
            <option value="by_date"><?php echo $_smarty_tpl->__("by_date");?>
</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field" id="by_period">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_period");?>
</label>
    <span class="ty-control-group__item">
        <select name="product_data[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
][extra][period_options][by_period]">
            <option>1 <?php echo $_smarty_tpl->__("month");?>
</option>
            <option>3 <?php echo $_smarty_tpl->__("months");?>
</option>
            <option>6 <?php echo $_smarty_tpl->__("months");?>
</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field hidden" id="by_date">
    <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("select_date");?>
</label>
    <span class="ty-control-group__item">
        <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_order_date",'date_name'=>"product_data[".((string)$_smarty_tpl->tpl_vars['obj_id']->value)."][extra][period_options][by_date]",'date_val'=>date("Y-m-d")), 0);?>

    </span>
</div>

<?php }?><?php }} ?>
