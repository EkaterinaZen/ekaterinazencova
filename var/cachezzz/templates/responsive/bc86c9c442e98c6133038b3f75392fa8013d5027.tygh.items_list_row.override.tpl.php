<?php /* Smarty version Smarty-3.1.21, created on 2015-11-13 04:31:41
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/order_period/hooks/orders/items_list_row.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:988357027564058b07509e2-48838292%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bc86c9c442e98c6133038b3f75392fa8013d5027' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/templates/addons/order_period/hooks/orders/items_list_row.override.tpl',
      1 => 1447378304,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '988357027564058b07509e2-48838292',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_564058b081af48_51257963',
  'variables' => 
  array (
    'runtime' => 0,
    'product' => 0,
    'order_info' => 0,
    'period_item' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_564058b081af48_51257963')) {function content_564058b081af48_51257963($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('download','sku','free','free','download','sku','free','free'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['product']->value['extra']['parent']) {?>
	<?php  $_smarty_tpl->tpl_vars["period_item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["period_item"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['extra']['my_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["period_item"]->key => $_smarty_tpl->tpl_vars["period_item"]->value) {
$_smarty_tpl->tpl_vars["period_item"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["period_item"]->key;
?>
		<tr class="ty-valign-top">
			<td>
				<?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
					<?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>

						<?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['is_edp']=="Y") {?>
								<div class="ty-right"><a href="<?php echo htmlspecialchars(fn_url("orders.order_downloads?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">[<?php echo $_smarty_tpl->__("download");?>
]</a></div>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
								<div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
							<?php }?>
							<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

								<?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {
echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options'],'inline_option'=>true), 0);
}?>
							<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			</td>
			<td class="ty-right">
				<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->__("free");
} else {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['original_price']), 0);
}?>
			</td>
			<td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
			<?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
				 <td class="ty-right">
					 <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discount']*($_smarty_tpl->tpl_vars['period_item']->value['amount']/$_smarty_tpl->tpl_vars['product']->value['amount'])), 0);
} else { ?>-<?php }?>
				 </td>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
				 <td class="ty-center">
					 <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['tax_value'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_value']), 0);
} else { ?>-<?php }?>
				 </td>
			<?php }?>
			 <td class="ty-right">
				 &nbsp;<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->__("free");
} else {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['period_item']->value['amount']*$_smarty_tpl->tpl_vars['product']->value['original_price']), 0);
}?>
			 </td>
			 <td class="ty-right">
				 &nbsp;<?php if ($_smarty_tpl->tpl_vars['period_item']->value['by_period']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['by_period'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['by_date'], ENT_QUOTES, 'UTF-8');
}?>
			 </td>
		</tr>
	<?php } ?>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/order_period/hooks/orders/items_list_row.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/order_period/hooks/orders/items_list_row.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['product']->value['extra']['parent']) {?>
	<?php  $_smarty_tpl->tpl_vars["period_item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["period_item"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['extra']['my_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["period_item"]->key => $_smarty_tpl->tpl_vars["period_item"]->value) {
$_smarty_tpl->tpl_vars["period_item"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["period_item"]->key;
?>
		<tr class="ty-valign-top">
			<td>
				<?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
					<?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>

						<?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['is_edp']=="Y") {?>
								<div class="ty-right"><a href="<?php echo htmlspecialchars(fn_url("orders.order_downloads?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">[<?php echo $_smarty_tpl->__("download");?>
]</a></div>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
								<div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
							<?php }?>
							<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

								<?php if ($_smarty_tpl->tpl_vars['product']->value['product_options']) {
echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['product']->value['product_options'],'inline_option'=>true), 0);
}?>
							<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			</td>
			<td class="ty-right">
				<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->__("free");
} else {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['original_price']), 0);
}?>
			</td>
			<td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
			<?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
				 <td class="ty-right">
					 <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discount']*($_smarty_tpl->tpl_vars['period_item']->value['amount']/$_smarty_tpl->tpl_vars['product']->value['amount'])), 0);
} else { ?>-<?php }?>
				 </td>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
				 <td class="ty-center">
					 <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['tax_value'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_value']), 0);
} else { ?>-<?php }?>
				 </td>
			<?php }?>
			 <td class="ty-right">
				 &nbsp;<?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->__("free");
} else {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['period_item']->value['amount']*$_smarty_tpl->tpl_vars['product']->value['original_price']), 0);
}?>
			 </td>
			 <td class="ty-right">
				 &nbsp;<?php if ($_smarty_tpl->tpl_vars['period_item']->value['by_period']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['by_period'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['period_item']->value['by_date'], ENT_QUOTES, 'UTF-8');
}?>
			 </td>
		</tr>
	<?php } ?>
<?php }
}?><?php }} ?>
