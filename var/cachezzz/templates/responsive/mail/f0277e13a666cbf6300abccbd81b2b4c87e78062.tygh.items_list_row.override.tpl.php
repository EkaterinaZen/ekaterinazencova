<?php /* Smarty version Smarty-3.1.21, created on 2015-11-09 09:37:46
         compiled from "/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131656409656403f3a7bbbb8-79500905%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f0277e13a666cbf6300abccbd81b2b4c87e78062' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/themes/responsive/mail/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl',
      1 => 1446489755,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '131656409656403f3a7bbbb8-79500905',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'oi' => 0,
    'order_info' => 0,
    'sub_oi' => 0,
    'conf_price' => 0,
    'conf_discount' => 0,
    'conf_tax' => 0,
    'conf_subtotal' => 0,
    'settings' => 0,
    '_colspan' => 0,
    'conf_oi' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56403f3a9c2ef0_16617400',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56403f3a9c2ef0_16617400')) {function content_56403f3a9c2ef0_16617400($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/ezencova/public_html/cscart/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_block_hook')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('sku','buy_together','product','amount','unit_price','discount','tax','subtotal','deleted_product','sku','sku','buy_together','product','amount','unit_price','discount','tax','subtotal','deleted_product','sku'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['oi']->value['extra']['buy_together']) {?>
    <?php $_smarty_tpl->tpl_vars["conf_oi"] = new Smarty_variable($_smarty_tpl->tpl_vars['oi']->value, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_price"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['price'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_subtotal"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_discount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_tax"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    
    
    <?php  $_smarty_tpl->tpl_vars["sub_oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sub_oi"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sub_oi"]->key => $_smarty_tpl->tpl_vars["sub_oi"]->value) {
$_smarty_tpl->tpl_vars["sub_oi"]->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['oi']->value['cart_id']) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_function_math(array('equation'=>"item_price * amount + conf_price",'item_price'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['price'])===null||$tmp==='' ? "0" : $tmp),'amount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['min_qty'])===null||$tmp==='' ? "1" : $tmp),'conf_price'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_price']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['oi']->value['price'] : $tmp),'assign'=>"conf_price"),$_smarty_tpl);?>
    
            <?php echo smarty_function_math(array('equation'=>"discount + conf_discount",'discount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp),'conf_discount'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_discount']->value)===null||$tmp==='' ? "0" : $tmp),'assign'=>"conf_discount"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"tax + conf_tax",'tax'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp),'conf_tax'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_tax']->value)===null||$tmp==='' ? "0" : $tmp),'assign'=>"conf_tax"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"subtotal + conf_subtotal",'subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp),'conf_subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_subtotal']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['oi']->value['display_subtotal'] : $tmp),'assign'=>"conf_subtotal"),$_smarty_tpl);?>

        <?php }?>
    <?php } ?>
    <tr>
        <td style="padding: 5px 10px; background-color: #ffffff;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product'], ENT_QUOTES, 'UTF-8');?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?><p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div style="padding-top: 1px; padding-bottom: 2px;"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?></td>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: center;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_price']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</td>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if (floatval($_smarty_tpl->tpl_vars['conf_discount']->value)) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_discount']->value), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if ($_smarty_tpl->tpl_vars['conf_tax']->value) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_tax']->value), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
        <?php }?>

        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value), 0);?>
</b>&nbsp;</td>
    </tr>
    <?php if (Smarty::$_smarty_vars['capture']['is_conf']) {?>
    <tr>
        <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable("4", null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {
$_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);
}?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {
$_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);
}?>
        <td style="padding: 5px 10px; background-color: #ffffff;" colspan="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_colspan']->value, ENT_QUOTES, 'UTF-8');?>
">
            <p><?php echo $_smarty_tpl->__("buy_together");?>
:</p>


        <table width="100%" cellpadding="0" cellspacing="1" style="background-color: #dddddd;">
        <tr>
            <th width="70%" style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("product");?>
</th>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("amount");?>
</th>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("unit_price");?>
</th>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("discount");?>
</th>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("tax");?>
</th>
            <?php }?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("subtotal");?>
</th>
        </tr>
        <?php  $_smarty_tpl->tpl_vars["oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["oi"]->_loop = false;
 $_smarty_tpl->tpl_vars["sub_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["oi"]->key => $_smarty_tpl->tpl_vars["oi"]->value) {
$_smarty_tpl->tpl_vars["oi"]->_loop = true;
 $_smarty_tpl->tpl_vars["sub_key"]->value = $_smarty_tpl->tpl_vars["oi"]->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['conf_oi']->value['cart_id']) {?>
        <tr>
            <td style="padding: 5px 10px; background-color: #ffffff;"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['product'])===null||$tmp==='' ? $_smarty_tpl->__("deleted_product") : $tmp), ENT_QUOTES, 'UTF-8');?>

                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?><p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div style="padding-top: 1px; padding-bottom: 2px;"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?>
            </td>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: center;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['price']), 0);?>
</td>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if (floatval($_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['extra']['discount']), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if ($_smarty_tpl->tpl_vars['oi']->value['tax_value']) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['tax_value']), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
            <?php }?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['display_subtotal']), 0);?>
&nbsp;</td>
        </tr>
        <?php }?>
        <?php } ?>
        </table>
    </tr>
    <?php }?>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/buy_together/hooks/orders/items_list_row.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/buy_together/hooks/orders/items_list_row.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['oi']->value['extra']['buy_together']) {?>
    <?php $_smarty_tpl->tpl_vars["conf_oi"] = new Smarty_variable($_smarty_tpl->tpl_vars['oi']->value, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_price"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['price'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_subtotal"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_discount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_tax"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    
    
    <?php  $_smarty_tpl->tpl_vars["sub_oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sub_oi"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sub_oi"]->key => $_smarty_tpl->tpl_vars["sub_oi"]->value) {
$_smarty_tpl->tpl_vars["sub_oi"]->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['oi']->value['cart_id']) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_function_math(array('equation'=>"item_price * amount + conf_price",'item_price'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['price'])===null||$tmp==='' ? "0" : $tmp),'amount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['min_qty'])===null||$tmp==='' ? "1" : $tmp),'conf_price'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_price']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['oi']->value['price'] : $tmp),'assign'=>"conf_price"),$_smarty_tpl);?>
    
            <?php echo smarty_function_math(array('equation'=>"discount + conf_discount",'discount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp),'conf_discount'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_discount']->value)===null||$tmp==='' ? "0" : $tmp),'assign'=>"conf_discount"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"tax + conf_tax",'tax'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp),'conf_tax'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_tax']->value)===null||$tmp==='' ? "0" : $tmp),'assign'=>"conf_tax"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"subtotal + conf_subtotal",'subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp),'conf_subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_subtotal']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['oi']->value['display_subtotal'] : $tmp),'assign'=>"conf_subtotal"),$_smarty_tpl);?>

        <?php }?>
    <?php } ?>
    <tr>
        <td style="padding: 5px 10px; background-color: #ffffff;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product'], ENT_QUOTES, 'UTF-8');?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?><p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div style="padding-top: 1px; padding-bottom: 2px;"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?></td>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: center;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_price']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</td>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if (floatval($_smarty_tpl->tpl_vars['conf_discount']->value)) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_discount']->value), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if ($_smarty_tpl->tpl_vars['conf_tax']->value) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_tax']->value), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
        <?php }?>

        <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['conf_subtotal']->value), 0);?>
</b>&nbsp;</td>
    </tr>
    <?php if (Smarty::$_smarty_vars['capture']['is_conf']) {?>
    <tr>
        <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable("4", null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {
$_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);
}?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {
$_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);
}?>
        <td style="padding: 5px 10px; background-color: #ffffff;" colspan="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_colspan']->value, ENT_QUOTES, 'UTF-8');?>
">
            <p><?php echo $_smarty_tpl->__("buy_together");?>
:</p>


        <table width="100%" cellpadding="0" cellspacing="1" style="background-color: #dddddd;">
        <tr>
            <th width="70%" style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("product");?>
</th>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("amount");?>
</th>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("unit_price");?>
</th>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("discount");?>
</th>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("tax");?>
</th>
            <?php }?>
            <th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap;"><?php echo $_smarty_tpl->__("subtotal");?>
</th>
        </tr>
        <?php  $_smarty_tpl->tpl_vars["oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["oi"]->_loop = false;
 $_smarty_tpl->tpl_vars["sub_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["oi"]->key => $_smarty_tpl->tpl_vars["oi"]->value) {
$_smarty_tpl->tpl_vars["oi"]->_loop = true;
 $_smarty_tpl->tpl_vars["sub_key"]->value = $_smarty_tpl->tpl_vars["oi"]->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['conf_oi']->value['cart_id']) {?>
        <tr>
            <td style="padding: 5px 10px; background-color: #ffffff;"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['product'])===null||$tmp==='' ? $_smarty_tpl->__("deleted_product") : $tmp), ENT_QUOTES, 'UTF-8');?>

                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?><p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div style="padding-top: 1px; padding-bottom: 2px;"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?>
            </td>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: center;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</td>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['price']), 0);?>
</td>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if (floatval($_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['extra']['discount']), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php if ($_smarty_tpl->tpl_vars['oi']->value['tax_value']) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['tax_value']), 0);
} else { ?>&nbsp;-&nbsp;<?php }?></td>
            <?php }?>
            <td style="padding: 5px 10px; background-color: #ffffff; text-align: right;"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['display_subtotal']), 0);?>
&nbsp;</td>
        </tr>
        <?php }?>
        <?php } ?>
        </table>
    </tr>
    <?php }?>
<?php }
}?><?php }} ?>
