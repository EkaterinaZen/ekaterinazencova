<?php /* Smarty version Smarty-3.1.21, created on 2015-11-09 11:39:43
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:137934088956405bcff35d23-11124635%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd19316dc400925cd554610e76137921e76fe8e9' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/buy_together/hooks/orders/items_list_row.override.tpl',
      1 => 1441800576,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '137934088956405bcff35d23-11124635',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oi' => 0,
    'order_info' => 0,
    'sub_oi' => 0,
    'conf_price' => 0,
    'conf_discount' => 0,
    'conf_tax' => 0,
    'conf_subtotal' => 0,
    'product_key' => 0,
    'settings' => 0,
    '_colspan' => 0,
    'c_oi' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56405bd00d7c47_88876715',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56405bd00d7c47_88876715')) {function content_56405bd00d7c47_88876715($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/ezencova/public_html/cscart/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_block_hook')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/ezencova/public_html/cscart/app/functions/smarty_plugins/modifier.truncate.php';
?><?php
fn_preload_lang_vars(array('collapse_sublist_of_items','sku','shipped','buy_together','sku','shipped'));
?>
<?php if ($_smarty_tpl->tpl_vars['oi']->value['extra']['buy_together']) {?>
    <?php $_smarty_tpl->tpl_vars["conf_price"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['price'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_subtotal"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['display_subtotal'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_discount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars["conf_tax"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp), null, 0);?>


    <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable(4, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["c_oi"] = new Smarty_variable($_smarty_tpl->tpl_vars['oi']->value, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars["sub_oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sub_oi"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sub_oi"]->key => $_smarty_tpl->tpl_vars["sub_oi"]->value) {
$_smarty_tpl->tpl_vars["sub_oi"]->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['oi']->value['cart_id']) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("is_conf", null, null); ob_start(); ?>1<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_function_math(array('equation'=>"item_price * amount + conf_price",'item_price'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['price'])===null||$tmp==='' ? "0" : $tmp),'amount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['min_qty'])===null||$tmp==='' ? "1" : $tmp),'conf_price'=>$_smarty_tpl->tpl_vars['conf_price']->value,'assign'=>"conf_price"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"discount + conf_discount",'discount'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['extra']['discount'])===null||$tmp==='' ? "0" : $tmp),'conf_discount'=>$_smarty_tpl->tpl_vars['conf_discount']->value,'assign'=>"conf_discount"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"tax + conf_tax",'tax'=>(($tmp = @$_smarty_tpl->tpl_vars['sub_oi']->value['tax_value'])===null||$tmp==='' ? "0" : $tmp),'conf_tax'=>$_smarty_tpl->tpl_vars['conf_tax']->value,'assign'=>"conf_tax"),$_smarty_tpl);?>

            <?php echo smarty_function_math(array('equation'=>"subtotal + conf_subtotal",'subtotal'=>$_smarty_tpl->tpl_vars['sub_oi']->value['display_subtotal'],'conf_subtotal'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_subtotal']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['oi']->value['display_subtotal'] : $tmp),'assign'=>"conf_subtotal"),$_smarty_tpl);?>

        <?php }?>
    <?php } ?>

    <?php $_smarty_tpl->tpl_vars["product_key"] = new Smarty_variable(uniqid("gc_"), null, 0);?>

    <tr valign="top">
        <td>
            <div class="pull-left">
                <i id="on_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_key']->value, ENT_QUOTES, 'UTF-8');?>
" class="hand cm-combination exicon-expand"></i>
                <i title="<?php echo $_smarty_tpl->__("collapse_sublist_of_items");?>
" id="off_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_key']->value, ENT_QUOTES, 'UTF-8');?>
" class="hand cm-combination hidden exicon-collapse"></i>
            </div>
            <a href="<?php echo htmlspecialchars(fn_url("products.update?product_id=".((string)$_smarty_tpl->tpl_vars['oi']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product'], ENT_QUOTES, 'UTF-8');?>
</a>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?></p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div class="options-info"><?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?>
        </td>
        <td class="nowrap"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_price']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</td>
        <td class="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>

            <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['use_shipments']=="Y"&&$_smarty_tpl->tpl_vars['oi']->value['shipped_amount']>0) {?>
                <p><span class="small-note">(<strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['shipped_amount'], ENT_QUOTES, 'UTF-8');?>
</strong>&nbsp;<?php echo $_smarty_tpl->__("shipped");?>
)</span></p>
            <?php }?>
        </td>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
        <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
        <td class="right nowrap">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_discount']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</td>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
        <?php $_smarty_tpl->tpl_vars["_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['_colspan']->value+1, null, 0);?>
        <td class="nowrap">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_tax']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</td>
        <?php }?>
        <td class="right"><strong><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['conf_subtotal']->value)===null||$tmp==='' ? 0 : $tmp)), 0);?>
</strong></td>
    </tr>
    <?php if (Smarty::$_smarty_vars['capture']['is_conf']) {?>
        <tr class="row-more row-gray hidden" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_key']->value, ENT_QUOTES, 'UTF-8');?>
" valign="top">
            <td colspan="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_colspan']->value, ENT_QUOTES, 'UTF-8');?>
">
                <p><?php echo $_smarty_tpl->__("buy_together");?>
:</p>
                <table width="100%" class="table-condensed">
                <?php  $_smarty_tpl->tpl_vars["oi"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["oi"]->_loop = false;
 $_smarty_tpl->tpl_vars["sub_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["oi"]->key => $_smarty_tpl->tpl_vars["oi"]->value) {
$_smarty_tpl->tpl_vars["oi"]->_loop = true;
 $_smarty_tpl->tpl_vars["sub_key"]->value = $_smarty_tpl->tpl_vars["oi"]->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']&&$_smarty_tpl->tpl_vars['oi']->value['extra']['parent']['buy_together']==$_smarty_tpl->tpl_vars['c_oi']->value['cart_id']) {?>
                    <tr>
                        <td width="50%">
                            <a href="<?php echo htmlspecialchars(fn_url("products.update?product_id=".((string)$_smarty_tpl->tpl_vars['oi']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['oi']->value['product'],50,"...",true), ENT_QUOTES, 'UTF-8');?>
</a>&nbsp;
                            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_code']) {?>
                                <p><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</p>
                            <?php }?>
                            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:product_info")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:product_info"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                            <?php if ($_smarty_tpl->tpl_vars['oi']->value['product_options']) {?><div style="padding-top: 1px; padding-bottom: 2px;">&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/options_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_options'=>$_smarty_tpl->tpl_vars['oi']->value['product_options']), 0);?>
</div><?php }?>
                            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:product_info"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </td>
                        <td width="10%" class="center nowrap">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['price']), 0);?>
</td>
                        <td width="10%" class="center nowrap">
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['amount'], ENT_QUOTES, 'UTF-8');?>

                            <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['use_shipments']=="Y"&&$_smarty_tpl->tpl_vars['oi']->value['shipped_amount']) {?>
                                <p><span class="small-note">(<strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oi']->value['shipped_amount'], ENT_QUOTES, 'UTF-8');?>
</strong>&nbsp;<?php echo $_smarty_tpl->__("shipped");?>
)</span></p>
                            <?php }?>
                        </td>
                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['use_discount']) {?>
                            <td width="5%" class="right nowrap">
                                <?php if (floatval($_smarty_tpl->tpl_vars['oi']->value['extra']['discount'])) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['extra']['discount']), 0);
} else { ?>-<?php }?></td>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']&&$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']!="subtotal") {?>
                            <td width="10%" class="center nowrap">
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['tax_value']), 0);?>
</td>
                        <?php }?>
                        <td width="10%" class="right nowrap">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['oi']->value['display_subtotal']), 0);?>
</td>
                    </tr>
                    <?php }?>
                <?php } ?>
                </table>
            </td>
        </tr>
    </tr>
    <?php }?>
<?php }?><?php }} ?>
