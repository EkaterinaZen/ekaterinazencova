<?php /* Smarty version Smarty-3.1.21, created on 2015-11-17 11:48:35
         compiled from "/home/ezencova/public_html/cscart/design/backend/templates/addons/admin_page/views/admin_page/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3765999745645c3eb9ce1d3-73478654%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a5a9d632e43fbb492e8ad5d2efacdb725fd3b47' => 
    array (
      0 => '/home/ezencova/public_html/cscart/design/backend/templates/addons/admin_page/views/admin_page/update.tpl',
      1 => 1447750112,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3765999745645c3eb9ce1d3-73478654',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5645c3ebc2de89_91764255',
  'variables' => 
  array (
    'entity_data' => 0,
    'id' => 0,
    'selected_section' => 0,
    'settings' => 0,
    'runtime' => 0,
    '_title' => 0,
    'redirect_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5645c3ebc2de89_91764255')) {function content_5645c3ebc2de89_91764255($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('title','position','comment','creation_date','edit_date','new_profile','editing_profile','delete','create'));
?>
<?php if ($_smarty_tpl->tpl_vars['entity_data']->value) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['entity_data']->value['entity_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable(0, null, 0);?>
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("addons/admin_page/views/admin_page/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form name="admin_page_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit form-table">
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
    <input type="hidden" name="entity_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" class="cm-no-hide-input" name="selected_section" id="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_section']->value, ENT_QUOTES, 'UTF-8');?>
" />
        
    <div id="content_general"> 
        <div class="control-group cm-no-hide-input">
            <label for="entity_title" class="control-label cm-required"><?php echo $_smarty_tpl->__("title");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="entity_data[title]" id="entity_title" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['entity_data']->value['title'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        
          <div class="control-group cm-no-hide-input">
            <label for="entity_position" class="control-label cm-required"><?php echo $_smarty_tpl->__("position");?>
</label>
            <div class="controls">
                <input class="input-large" type="text" name="entity_data[position]" id="entity_position" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['entity_data']->value['position'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
             
        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_entity_comment"><?php echo $_smarty_tpl->__("comment");?>
:</label>
            <div class="controls">
                <textarea id="elm_entity_comment" name="entity_data[comment]" cols="55" rows="8" class="cm-wysiwyg input-large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['entity_data']->value['comment'], ENT_QUOTES, 'UTF-8');?>
</textarea>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="elm_entity_date"><?php echo $_smarty_tpl->__("creation_date");?>
</label>
            <div class="controls">
                <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_entity_date",'date_name'=>"entity_data[date]",'date_val'=>@constant('TIME'),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

            </div>
        </div>
           
        <div class="control-group">
            <label class="control-label" for="elm_entity_edit_date"><?php echo $_smarty_tpl->__("edit_date");?>
</label>
            <div class="controls">
                <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_entity_edit_date",'date_name'=>"entity_data[edit_date]",'date_val'=>@constant('TIME'),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

            </div>
        </div>                  
          
        <?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"entity_data[status]",'id'=>"elm_entity_status",'obj'=>$_smarty_tpl->tpl_vars['entity_data']->value,'hidden'=>true), 0);?>

    </div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'group_name'=>$_smarty_tpl->tpl_vars['runtime']->value['controller'],'active_tab'=>$_smarty_tpl->tpl_vars['selected_section']->value,'track'=>true), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php ob_start();
echo $_smarty_tpl->__("new_profile");
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["_title"] = new Smarty_variable($_tmp1, null, 0);?>
<?php } else { ?>
    <?php ob_start();
echo $_smarty_tpl->__("editing_profile");
$_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["_title"] = new Smarty_variable($_tmp2.": ".((string)$_smarty_tpl->tpl_vars['entity_data']->value['title']), null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars['_title'] = new Smarty_variable(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['_title']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars["redirect_url"] = new Smarty_variable("admin_page.manage", null, 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>__("delete"),'class'=>"cm-confirm cm-post",'href'=>"admin_page.delete?entity_id=".((string)$_smarty_tpl->tpl_vars['id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['redirect_url']->value)));?>
</li>     
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php if ($_smarty_tpl->tpl_vars['id']->value&&trim(Smarty::$_smarty_vars['capture']['tools_list'])!=='') {?>
        <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

    <?php }?>
    <div class="btn-group btn-hover dropleft">
        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_changes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"dropdown-toggle",'but_role'=>"submit-link",'but_name'=>"dispatch[admin_page.".((string)$_smarty_tpl->tpl_vars['runtime']->value['mode'])."]",'but_target_form'=>"admin_page_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>__("create"),'but_meta'=>"dropdown-toggle",'but_role'=>"submit-link",'but_name'=>"dispatch[admin_page.".((string)$_smarty_tpl->tpl_vars['runtime']->value['mode'])."]",'but_target_form'=>"admin_page_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

        <?php }?>
    </div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['_title']->value,'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons']), 0);?>

</form>
<?php }} ?>
