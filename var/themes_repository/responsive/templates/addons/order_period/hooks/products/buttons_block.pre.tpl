<div class="ty-control-group product-list-field">
    <label class="ty-control-group__label">{__("select_type")}</label>
    <span class="ty-control-group__item">
        <select id="change_period" name="product_data[{$obj_id}][by_type]">
            <option value="by_period">{__("by_period")}</option>
            <option value="by_date">{__("by_date")}</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field" id="by_period">
    <label class="ty-control-group__label">{__("select_period")}</label>
    <span class="ty-control-group__item">
        <select name="product_data[{$obj_id}][by_period]">
            <option>1 {__("month")}</option>
            <option>3 {__("months")}</option>
            <option>6 {__("months")}</option>
        </select>
    </span>
</div>
<div class="ty-control-group product-list-field hidden" id="by_date">
    <label class="ty-control-group__label">{__("select_date")}</label>
    <span class="ty-control-group__item">
        {include file="common/calendar.tpl" date_id="elm_order_date" date_name="product_data[{$obj_id}][by_date]" date_val="Y-m-d"|date}
    </span>
</div>

