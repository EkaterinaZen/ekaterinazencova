<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_orders_statistics_dashboard_get_graphs_data($time_from, $time_to, $graphs, $graph_tabs, $is_day)
{ 
    $graph_tabs['orders_statistics'] = array (
        'title' => __('orders_statistics'),
        'js' => true
    );

}

function fn_get_orders_by_period()
{       
    $statistics['this_week'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("last Monday") . " AND " . strtotime("+1 week", strtotime("last Monday")) . " AND status IN ('C','O','P','F') GROUP BY status"); 
    
    $statistics['last_week'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("-1 week", strtotime("last Monday")) . " AND " . strtotime("last Monday") . " AND status IN ('C','O','P','F') GROUP BY status");
      
    $statistics['this_month'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("first day of this month 00:00") . " AND " . strtotime("last day of this month 00:00") . " AND status IN ('C','O','P','F') GROUP BY status");
    
    $statistics['last_month'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("first day of last month 00:00") . " AND " . strtotime("first day of this month 00:00") . " AND status IN ('C','O','P','F') GROUP BY status");
      
    $statistics['this_year'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("1 January this year") . " AND " . strtotime("1 January next year") . " AND status IN ('C','O','P','F') GROUP BY status");
 
    $statistics['last_year'] =  db_get_array("SELECT status, count(status) AS count, sum(total) AS total FROM ?:orders WHERE timestamp BETWEEN " . strtotime("1 January last year") . " AND " . strtotime("1 January this year") . " AND status IN ('C','O','P','F') GROUP BY status");
    
    return $statistics;
}

function fn_get_status_description($statuses)
{ 
    foreach ($statuses as $key => $item)
    {
        $new_statuses[$key] = $item['description'];
    }
    return $new_statuses;  
}

function fn_get_mixed_statistics($statistics)
{
    foreach ($statistics as $key => $period)
    {
        foreach ($period as $fields)
        {
            $all_statistics[$fields['status']][$key] = array(
                                                            'total' => $fields['total'], 
                                                            'count' => $fields['count']
                                                          );
        } 
    }
    return $all_statistics;  
}

function fn_get_total_cost($statistics, $paid_statuses, $total_statuses)
{
    foreach ($statistics as $status => $item)
    {
        foreach ($item as $key => $fields)
        {
            if (in_array($status, $paid_statuses))
           // if (($status != 'F') && ($status != 'O'))
            {
                $total_paid[$key] = !empty($total_paid[$key]) ? $total_paid[$key] : 0;
                $total_paid[$key] += $fields['total'];             
            }
            
            if (in_array($status, $total_statuses))
            //if ($status != 'F')
            {
                $gross_total[$key] = !empty($gross_total[$key]) ? $total_paid[$key] : 0;
                $gross_total[$key] += $fields['total'];   
            }
        }
    }
    return array($total_paid, $gross_total);
}
