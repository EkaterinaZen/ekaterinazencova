<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\Tools\DateTimeHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'index') {
    
    $statistics = fn_get_orders_by_period();
    
    $statuses =  fn_get_statuses('O', array(), false, false, DESCR_SL);
        
    $four_statuses = fn_get_status_description($statuses);  
  
    $orders_statistics = fn_get_mixed_statistics($statistics);   
  
    list($gross_total, $total_paid) = fn_get_total_cost($orders_statistics, array('C','P'), array('C','O','P'));
    
    Tygh::$app['view']->assign('four_statuses', $four_statuses);    
    Tygh::$app['view']->assign('orders_statistics', $orders_statistics);
    Tygh::$app['view']->assign('gross_total', $gross_total);
    Tygh::$app['view']->assign('total_paid', $total_paid);
}
