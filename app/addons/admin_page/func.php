<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_delete_entity($entity_id)
{
    $result = db_query("DELETE FROM ?:my_entities WHERE entity_id = ?i", $entity_id);
    return $result;
}

function fn_get_entity_info($entity_id)
{
    $result = db_get_row("SELECT * FROM ?:my_entities WHERE entity_id = ?i", $entity_id);
    return $result;
}

function fn_get_entities($params = array())
{
    $condition = '';
    if (isset ($params['title']) && $params['title'])
        $condition .= " AND `title` = '" . $params['title'] . "'";
    if (isset ($params['position']) && $params['position'])
        $condition .= " AND `position` = '" . $params['position'] . "'";
    if (isset ($params['status']) && $params['status'])
        $condition .= " AND `status` = '" . $params['status'] . "'";
    if (isset ($params['comment']) && $params['comment'])
        $condition .= " AND `comment` LIKE '%" . trim($params['comment']) . "%'";
    if (isset ($params['date']) && $params['date'])
        $condition .= " AND DATE(`date`) >= '" . $params['date'] . "'";
    if (isset ($params['edit_date']) && $params['edit_date'])
        $condition .= " AND DATE(`edit_date`) >= '" . $params['edit_date'] . "'";
    
    $result = db_get_array("SELECT * FROM ?:my_entities WHERE 1" . $condition);
    return $result;
}

function fn_update_entity($entity_data, $entity_id = false)
{
    $entity_data['date'] = date('Y-m-d H:i:s', strtotime($entity_data['date']));
    $entity_data['edit_date'] = date('Y-m-d H:i:s', strtotime($entity_data['edit_date']));
    if (!$entity_id)
    {
        $result = db_query("INSERT INTO ?:my_entities ?e", $entity_data);
    } else {
        $update = db_query("UPDATE ?:my_entities SET ?u WHERE entity_id = ?i",$entity_data, $entity_id);
        if (!empty($update))
        {
            $result = $entity_id;
        }
    }
     
    return $result;
}


function fn_get_entity_data($params, $items_per_page = 0)
{
    // Init view params
    $params = LastView::instance()->update('entity_data', $params);

    // Set default values to input params
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $fields_list = array(
        '?:my_enities.entity_id',
        '?:my_entities.position',
        '?:my_entities.title',
        '?:my_entities.status',
        '?:my_entities.comment',
        '?:my_entities.date',
        '?:my_entities.edit_date',
    );

}
