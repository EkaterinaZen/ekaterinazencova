<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Session;
use Tygh\Mailer;
use Tygh\Api;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'm_delete') {

        if (!empty($_REQUEST['entity_ids'])) {
            foreach ($_REQUEST['entity_ids'] as $v) {
                fn_delete_entity($v);
            }
        }

        return array(CONTROLLER_STATUS_OK, 'admin_page.manage');
    }
    
   if ($mode == 'update' || $mode == 'add') {
        fn_trusted_vars("entity_data");
    
        $entity_id = !empty($_REQUEST['entity_id']) ? $_REQUEST['entity_id'] : 0;
        $mode = empty($_REQUEST['entity_id']) ? 'add' : 'update';
        fn_save_post_data('entity_data');
        
        if (isset($_REQUEST['entity_data'])){
            if ($_REQUEST['entity_id']!=0){
                $result = fn_update_entity($_REQUEST['entity_data'], $_REQUEST['entity_id']);
            } else {
                $result = fn_update_entity($_REQUEST['entity_data']);
            }
        }
        
        $redirect_params =  array(
            'entity_id' => $entity_id
        );

        if (!empty($_REQUEST['return_url'])) {
           $redirect_params['return_url'] = urlencode($_REQUEST['return_url']);
        }

        return array(CONTROLLER_STATUS_OK, 'admin_page' . (!empty($entity_id) ? '.update' : '.add') . '?' . http_build_query($redirect_params));
    }

    if ($mode == 'delete') {

        fn_delete_entity($_REQUEST['entity_id']);

        return array(CONTROLLER_STATUS_REDIRECT, 'admin_page.manage');

    }
    
    if ($mode == 'update_status') {

        $entity_data = fn_get_entity_info($_REQUEST['id']);
        if (!empty($entity_data)) {
            $result = db_query("UPDATE ?:my_entities SET status = ?s WHERE entity_id = ?i", $_REQUEST['status'], $_REQUEST['id']);
            if ($result && $_REQUEST['id'] != 1) {
                Tygh::$app['ajax']->assign('return_status', $entity_data['status']);
            }
        }

        exit;
    }
}

if ($mode == 'manage') {

    $entities = fn_get_entities();

    Tygh::$app['view']->assign('entities', $entities);
     
} elseif ($mode == 'update' || $mode == 'add') {
     
    $entity_id = !empty($_REQUEST['entity_id']) ? $_REQUEST['entity_id'] : 0;
    $entity_data = fn_get_entity_info($entity_id);
    $saved_entity_data = fn_restore_post_data('entity_data');
    if (!empty($saved_entity_data)) {
        $entity_data = fn_array_merge($entity_data, $saved_entity_data);
    }
    if ($mode == 'update') {
        if (empty($entity_data)) {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }
    }
    $entity_data['entity_id'] = empty($entity_data['entity_id']) ? (!empty($entity_id) ? $entity_id : 0) : $entity_data['entity_id'];
    Tygh::$app['view']->assign('entity_data', $entity_data);
}
