<?php
namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\Registry;
use Tygh\Settings;

class Comments extends AEntity
{
    public function index($id = '', $params = array())
    {
        
        if (!empty($id))
        {
            $data = fn_get_entity_info($id);
        } elseif (!empty($params)) {
            $data = fn_get_entities($params);
        } else {
            $data = fn_get_entities();
        }
        return array(
            'status' => Response::STATUS_OK,
            'data' => $data
        );
    }

    public function create($params)
    {
        if (!empty($params))
        {
            $entity_data['position'] = $params['position'];
            $entity_data['title'] = $params['title'];
            $entity_data['comment'] = $params['comment'];
            $entity_data['status'] = $params['status'];
            $entity_data['date'] = date('Y-m-d H:i:s');
            
            $id = fn_update_entity($entity_data);
            
            if (!empty($id))
            {
                $data = fn_get_entity_info($id);
                return array(   
                    'status' => Response::STATUS_CREATED,
                    'data' => $data
                );       
            } else {
                return array(
                    'status' => Response::STATUS_NO_CONTENT,
                );
            }
        } else {
            return array(
                'status' => Response::STATUS_BAD_REQUEST,
            );
        }
    }

    public function update($id, $params)
    {
        if (!empty($params) && !empty($id))
        {
            $entity_data['position'] = $params['position'];
            $entity_data['title'] = $params['title'];
            $entity_data['comment'] = $params['comment'];
            $entity_data['status'] = $params['status'];
            $entity_data['date'] = date('Y-m-d H:i:s');
                
            $entity_id = fn_update_entity($entity_data, $id);
            $data = fn_get_entity_info($entity_id);
            
            if (!empty($data)) 
            {         
                return array(
                    'status' => Response::STATUS_OK,
                    'data' => array()
                );
                    
            } else {    
                return array(
                    'status' => Response::STATUS_NO_CONTENT
                );
            }
        }
    }
    
    public function delete($id)
    {   
        if (!empty($id))
        {
             
            $result = fn_delete_entity($id);
            if (!empty($result))
            {
                return array(
                    'status' => Response::STATUS_NO_CONTENT
                );
            } else {
                return array(
                    'status' => Response::STATUS_BAD_REQUEST
                );
            } 
        } else {
            return array(
                'status' => Response::STATUS_BAD_REQUEST
            );
        }       
    }
    
    public function privileges()
    {
        return array(
            'create' => 'manage_privilege',
            'update' => 'manage_privilege',
            'delete' => 'manage_privilege',
            'index'  => 'view_privilege'
        );
    }
}
