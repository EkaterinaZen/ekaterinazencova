<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_order_period_pre_add_to_cart(&$product_data, &$cart, $auth, $update)
{
	foreach ($product_data as $key => &$one_product)
	{  
		if (isset($one_product['extra']['period_options']))
		{
		    $options = &$one_product['extra']['period_options'];
		    $field_name = $options['by_type'];
		    $field_value = $options[$field_name];
		    if (isset($options['by_period'])) unset($options['by_period']);
		    if (isset($options['by_date'])) unset($options['by_date']);
		    unset($options['by_type']);
		 	
		    $options[$field_name] = $field_value;
	        unset($options); 
			unset($one_product);
	    }
	}
}

function fn_order_period_add_to_cart(&$cart, $product_id, $_id)
{	
	if(!empty($cart['products'][$_id]['extra']['period_options']))
	{	
	    $old_options = $cart['products'][$_id]['extra']['product_options'];	    
	    $cart['products'][$_id]['extra']['product_options'] += $cart['products'][$_id]['extra']['period_options'];
	    $new_id = fn_generate_cart_id($product_id, $cart['products'][$_id]['extra'], false);
	    $cart['products'][$_id]['extra']['product_options'] = $old_options;
	
	    $cart_ids = array_keys($cart['products']);		
	    
	    if (in_array($new_id, $cart_ids))
	    {
		    $cart['products'][$new_id]['amount'] += $cart['products'][$_id]['amount'];
	    } else {
		    $cart['products'][$new_id] = $cart['products'][$_id];
	    }	
	    unset($cart['products'][$_id]);		
	}
}

function fn_order_period_calculate_cart_post($cart, $auth, $calculate_shipping, $calculate_taxes, $options_style, $apply_cart_promotions, &$cart_products, $product_groups)
{
    foreach ($cart['products'] as $key => $product)
    {
        if (!empty($product['extra']['period_options']))
        {
            $cart_products[$key]['extra']['period_options'] = $product['extra']['period_options'];
        }
    }
}
