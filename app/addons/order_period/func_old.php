<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_order_period_pre_add_to_cart(&$product_data, &$cart, $auth, $update){
	
	fn_print_die($product_data);
	reset($product_data);
	$first = key($product_data);
	$options = &$product_data[$first]['extra']['period_options'];
	
	if (isset($options['by_type'])){
		
		$field_name = $options['by_type'];
		$field_value = $options[$field_name];
		
		if (isset($options['by_period'])) unset($options['by_period']);
		if (isset($options['by_date'])) unset($options['by_date']);
		unset($options['by_type']);
		
		$options[$field_name] = $field_value;		
	}
	
	// $options['amount'] = $product_data[$first]['amount'];
	
	// Check if product options exist
	// if (isset($product_data[$first]['product_options'])) {
		// $options = $options + $product_data[$first]['product_options'];		
	// } else {
		// if (isset($options)){
			// $options = $options + fn_get_default_product_options($first);
		// } else {
			// $options = fn_get_default_product_options($first);
		// }
	// }	
	
	unset($options);
	
	// foreach ($product_data as &$p_data){
		// foreach ($cart['products'] as &$c_data){
			// if ($p_data['product_id'] == $c_data['product_id']){
				// $p_data['extra'] = $c_data['extra'];
			// }
			// unset($c_data);
		// }
		// unset($p_data);
	// }
}

function fn_order_period_add_to_cart(&$cart, $product_id, $_id){	
	
	if(!empty($cart['products'][$_id]['extra']['period_options']))
	{
		reset($cart['products'][$_id]['extra']['period_options']);
		$field_name = key($cart['products'][$_id]['extra']['period_options']);
		$field_value = $cart['products'][$_id]['extra']['period_options'][$field_name];
		
		$cart['products'][$_id]['extra']['product_options'][$field_name] = $field_value;
		$new_id = fn_generate_cart_id($product_id, $cart['products'][$_id]['extra'], false);
		unset($cart['products'][$_id]['product_options'][$field_name]);
		
		$cart_ids = array();		
		foreach($cart['products'] as $id => $product)
		{
			$cart_ids[] = $id;
		}
		
		if (in_array($new_id, $cart_ids))
		{
			$cart['products'][$new_id]['amount'] += $cart['products'][$_id]['amount'];
		} else {
			$cart['products'][$new_id] = $cart['products'][$_id];
			unset($cart['products'][$_id]);
		}	
	}
	
	
	
	// $my_extra = &$cart['products'][$_id]['extra'];
	
	// if (isset($my_extra)){
	
		// $my_extra['my_data'] = ($my_extra['my_data']) ? $my_extra['my_data'] : array(); 
		
		// if (isset($_REQUEST['product_data'][$product_id]['by_type'])){
			// $field_name = $_REQUEST['product_data'][$product_id]['by_type'];
			// if ($field_name == 'by_date'){
				// $field_value = strtotime($_REQUEST['product_data'][$product_id]['by_date']);			
			// } elseif ($field_name == 'by_period'){
				// $field_value = $_REQUEST['product_data'][$product_id]['by_period'];				
			// }
		// }
		
		// $found = false;
		// foreach ($my_extra['my_data'] as &$data){				
			// if ((isset($data['by_date']) && ($field_value == $data['by_date'])) || (isset($data['by_period']) && ($field_value == $data['by_period']))){
				// $found = true;
				// $data['amount'] += intval($_REQUEST['product_data'][$product_id]['amount']);
			// }
			// unset($data);
		// }
		
		// if (!$found){
			// $my_extra['my_data'][] = array(
				// $field_name => $field_value,
				// 'amount' => $_REQUEST['product_data'][$product_id]['amount']
			// );
		// }               
	// }

	// fn_print_r($cart['products'][$_id]);
	// fn_print_die($my_extra);
	 
	// unset($my_extra);
	
	// foreach ($cart['products'] as &$s_prod){
        // foreach ($_REQUEST['product_data'] as $r_prod){
            // if (intval($s_prod['product_id']) == intval($r_prod['product_id'])){
                // $s_prod['extra'][$r_prod['by_type']] = $r_prod[$r_prod['by_type']];
            // }
        // }
        // unset($s_prod);
    // }
}



function fn_order_period_place_order($order_id, $action, $order_status, $cart, $auth) {
	// $insert_values = array();

    // foreach ($cart['products'] as $one_key => $one_product){
		// if (isset($one_product['extra']['my_data'])){
			// foreach ($one_product['extra']['my_data'] as $key => $item){
				// if (isset($item['by_period'])){
					// $field_name = "by_period";
					// $field_value = $item['by_period'];
				// }                
				// if (isset($item['by_date'])){
					// $field_name = "by_date";
					// $field_value = date("Y-m-d", $item['by_date']);
				// }
				
				// $insert_values[] = array(
					// 'item_id' => $one_key,
					// 'order_id' => $order_id,
					// 'period_id' => $key,
					// $field_name => $field_value,
					// 'amount' => $item['amount']
				// );
			// }
		// }
	// }
	
	// fn_print_die($insert_values);
	
	// db_query('INSERT INTO ?:order_period ?m', $insert_values);
        /*if (isset($one_product['extra']['by_period'])){
            $field_name = "by_period";
            $field_value = $one_product['extra']['by_period'];
        }                
        if (isset($one_product['extra']['by_date'])){
            $field_name = "by_date";
            $field_value = date("Y-m-d", strtotime($one_product['extra']['by_date']));
        }
        $update_details[] = array(
            'item_id' => $key,
            'order_id' => $order_id,
			'data' => array(
				$field_name => $field_value
			)
        );
    }
		
    foreach($update_details as $product_details) {
        db_query('UPDATE ?:order_details SET ?u WHERE item_id = ?i AND order_id = ?i', $product_details['data'], $product_details['item_id'], $product_details['order_id']);
    }*/
}

function fn_order_period_calculate_cart_post($cart, $auth, $calculate_shipping, $calculate_taxes, $options_style, $apply_cart_promotions, &$cart_products, $product_groups)
{
    foreach ($cart['products'] as $key => $product)
    {
        if (!empty($product['extra']['period_options']))
        {
            $cart_products[$key]['extra']['period_options'] = $product['extra']['period_options'];
        }
    }
}
