<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_delete_staff($staff_id)
{
    $result = db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
    return $result;
}

function fn_get_staff_info($staff_id)
{
    $staff_data = db_get_row("SELECT * FROM ?:staff WHERE staff_id = ?i", $staff_id);
	$staff_data['main_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, true, DESCR_SL);	
	$users = db_get_array("SELECT user_id, firstname, lastname, email FROM ?:users");
	
	return array($staff_data, $users);
}

function fn_get_staff()
{
    $all_staff = db_get_array("SELECT * FROM ?:staff");
	$all_staff = $all_staff ?: array();
	
	foreach ($all_staff as &$staff)
	{
	    $staff['main_pair'] = fn_get_image_pairs($staff['staff_id'], 'staff', 'M', true, true, DESCR_SL);
	    unset($staff);
	}
	
	if (false)
	{
		$user_ids = array();
		foreach ($all_staff as $staff)
		{
			if (!empty($staff['user_id']))
			{
				$user_ids[] = $staff['user_id'];
			}
		}
		
		$users = array();
		if (!empty($user_ids))
		{
			$users = db_get_array("SELECT user_id, firstname, lastname, email FROM ?:users WHERE user_id IN (" . implode($user_ids, ', ') . ")");
		}
		
		foreach ($all_staff as &$staff)
		{
			foreach ($users as $user)
			{
				if ($staff['user_id'] == $user['user_id'])
				{
					if (empty($staff_data['first_name']) && !empty($user['firstname']))
					{
					    $staff_data['first_name'] = $user['firstname'];
					}
	                if (empty($staff_data['last_name']) && !empty($user['lastname']))
	                {
	                    $staff_data['last_name'] = $user['lastname'];
	                }
		            if (empty($staff_data['email']) && !empty($user['email']))
		            {
		                $staff_data['email'] = $user['email'];
		            }
				}	
			}
			unset($staff);
		}
	}
	
	return $all_staff;    
}

function fn_update_staff($staff_data, $staff_id = false)
{
    if (!empty($staff_id))
	{
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $staff_data, $staff_id);
		fn_attach_image_pairs('staff_main', 'staff', $staff_id, DESCR_SL);
    } else {
		$staff_id = db_query("INSERT INTO ?:staff ?e", $staff_data);
		fn_attach_image_pairs('staff_main', 'staff', $staff_id, DESCR_SL);
	}
	
	return $staff_id;
}
