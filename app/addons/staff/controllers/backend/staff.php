<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Session;
use Tygh\Mailer;
use Tygh\Api;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'get_email') {
        if (!empty($_REQUEST['staff_id']))
        {
            $staff = fn_get_staff_info($_REQUEST['staff_id']);
            
            if (!empty($staff['email']))
            {
                return "<a href=''>" . $staff['email'] . "</a>";
            } else {
                return "__{'no_email'}";
            }
        }
    }

    if ($mode == 'm_delete') {

        if (!empty($_REQUEST['staff_ids'])) {
            foreach ($_REQUEST['staff_ids'] as $v) {
                fn_delete_staff($v);
            }
        }

        return array(CONTROLLER_STATUS_OK, 'staff.manage');
    }
    
   if ($mode == 'update' || $mode == 'add') {
        fn_trusted_vars("staff_data");
    
        $staff_id = !empty($_REQUEST['staff_id']) ? $_REQUEST['staff_id'] : 0;
        $mode = empty($_REQUEST['staff_id']) ? 'add' : 'update';
        fn_save_post_data('staff_data');
        
        if (isset($_REQUEST['staff_data'])){
            if ($_REQUEST['staff_id'] != 0){
                fn_update_staff($_REQUEST['staff_data'], $_REQUEST['staff_id']);
            } else {
                $staff_id = fn_update_staff($_REQUEST['staff_data']);
            }
        }
        
        $redirect_params =  array(
            'staff_id' => $staff_id
        );

        if (!empty($_REQUEST['return_url'])) {
           $redirect_params['return_url'] = urlencode($_REQUEST['return_url']);
        }
		
        return array(CONTROLLER_STATUS_OK, 'staff' . (!empty($staff_id) ? '.update' : '.add') . '?' . http_build_query($redirect_params));
    }

    if ($mode == 'delete') {

        fn_delete_staff($_REQUEST['staff_id']);

        return array(CONTROLLER_STATUS_REDIRECT, 'staff.manage');

    }
    
    if ($mode == 'update_status') {

        $staff_data = fn_get_staff_info($_REQUEST['id']);
        if (!empty($staff_data)) {
            $result = db_query("UPDATE ?:staff SET status = ?s WHERE staff_id = ?i", $_REQUEST['status'], $_REQUEST['id']);
            if ($result && $_REQUEST['id'] != 1) {
                Tygh::$app['ajax']->assign('return_status', $staff_data['status']);
            }
        }

        exit;
    }
}

if ($mode == 'manage') {

    $staff_data = fn_get_staff();

    Tygh::$app['view']->assign('staff_data', $staff_data);
     
} elseif ($mode == 'update' || $mode == 'add') {
     
    $staff_id = !empty($_REQUEST['staff_id']) ? $_REQUEST['staff_id'] : 0;
    list($staff_data, $users) = fn_get_staff_info($staff_id);
    $saved_staff_data = fn_restore_post_data('staff_data');
    if (!empty($saved_staff_data)) {
        $staff_data = fn_array_merge($staff_data, $saved_staff_data);
    }
    if ($mode == 'update') {
        if (empty($staff_data)) {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }
    }
    $staff_data['staff_id'] = empty($staff_data['staff_id']) ? (!empty($staff_id) ? $staff_id : 0) : $staff_data['staff_id'];
    Tygh::$app['view']->assign('staff_data', $staff_data);
    Tygh::$app['view']->assign('users', $users);
}
