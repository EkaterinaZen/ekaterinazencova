<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_fields_sort_field_sorting()
{
	$sort_list = Registry::get('settings.Appearance.available_product_list_sortings');
	$keys_list = array_keys($sort_list);
	if (!in_array('date-asc', $keys_list)){
		$keys_list[] = 'date-asc';
	} 
	if (!in_array('date-desc', $keys_list)){
		$keys_list[] = 'date-desc';
	} 
	if (!in_array('location-asc', $keys_list)){
		$keys_list[] = 'location-asc';
	} 
	if (!in_array('location-desc', $keys_list)){
		$keys_list[] = 'location-desc';
	}
	Settings::instance()->updateValue('available_product_list_sortings', $keys_list, '', false, '1', true);			
}

function fn_fields_sort_products_sorting(&$sorting, $simple_mode)
{	
	if (!isset($sorting['date']))
	{ 
		$sorting['date'] = array('description' => __('none'), 'default_order' => 'asc');
    }
    if (!isset($sorting['location']))
	{ 
		$sorting['location'] = array('description' => __('none'), 'default_order' => 'asc');
    }
}

function fn_fields_sort_get_products(&$params, $fields, &$sortings, $condition, $join, $sorting, $group_by, $lang_code, $having)
{
	$sortings['date'] = 'field_date';
	$sortings['location'] = 'field_location';	
}

function fn_fields_sort_update_product_pre(&$product_data, $product_id, $lang_code, $can_update)
{
    if (!empty($product_data['field_date'])) {
        $product_data['field_date'] = fn_parse_date($product_data['field_date']);
    }
}
