<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_order_prefix_place_order($order_id)
{
	$prefix = Settings::instance()->getSettingDataByName('select_prefix');
	if (!empty($prefix)){
		$data = array(
			'order_prefix' => $prefix['value'] . $order_id
		);
	}
	db_query('UPDATE ?:orders SET ?u WHERE order_id = ?i', $data, $order_id);
}
