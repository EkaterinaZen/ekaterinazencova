<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_products_rand_add_sorting()
{
	$sort_list = Registry::get('settings.Appearance.available_product_list_sortings');
	$keys_list = array_keys($sort_list);
	if (!in_array('rand-asc', $keys_list)){
		$keys_list[] = 'rand-asc';
	}
	Settings::instance()->updateValue('available_product_list_sortings', $keys_list, '', false, '1', true);			
}

function fn_products_rand_products_sorting(&$sorting, $simple_mode)
{	
	if (!isset($sorting['rand']))
	{ 
		$sorting['rand'] = array('description' => __('none'), 'default_order' => 'asc', 'desc' => false);
    }
}

function fn_products_rand_get_products(&$params, $fields, &$sortings, $condition, $join, $sorting, $group_by, $lang_code, $having)
{
     $sortings['rand'] = 'RAND()';
}
