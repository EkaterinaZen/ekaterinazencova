<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_shipping_data_get_order_info(&$order, $additional_data)
{    
    if (isset($order['order_id']))
    {
        $fields = db_get_array("SELECT shipping_date, shipping_comment FROM ?:orders as o LEFT JOIN ?:order_descriptions as d ON o.order_id = d.order_id WHERE d.order_id = ?s AND d.lang_code = ?s", $order['order_id'], DESCR_SL);
        
        reset($fields);
        $el = key($fields);
                    
        if (isset($fields[$el]['shipping_date']))
        {
            $order['shipping_date'] = $fields[$el]['shipping_date'];
        }
        if (isset($fields[$el]['shipping_comment']))
        {
            $order['shipping_comment'] = $fields[$el]['shipping_comment'];
        }
    }
}
