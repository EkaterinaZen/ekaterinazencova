<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update_details') {
        
        if (isset($_REQUEST['update_order']['shipping_date'])){
            $fields = array ('shipping_date' => strtotime($_REQUEST['update_order']['shipping_date']));
            db_query("UPDATE ?:orders SET ?u WHERE order_id = ?i", $fields, $_REQUEST['order_id']);   
        }     
        
        if (isset($_REQUEST['update_order']['shipping_comment']))
        {
            $insert_order = array('order_id' => $_REQUEST['order_id'], 
                                  'shipping_comment' => $_REQUEST['update_order']['shipping_comment'],
                                  'lang_code' => DESCR_SL
                                 );
            $fields = array('shipping_comment' => $_REQUEST['update_order']['shipping_comment']);
        }
        
        
        $check_order = db_get_array("SELECT * FROM ?:order_descriptions WHERE order_id = ?i AND lang_code = ?s", $_REQUEST['order_id'], DESCR_SL);
        
        
        if (count($check_order) == 0)
        {
            db_query("INSERT INTO ?:order_descriptions ?e", $insert_order); 
        } else {
            db_query("UPDATE ?:order_descriptions SET ?u WHERE order_id = ?i", $fields, $_REQUEST['order_id']);  
        } 
    }
}

if ($mode == 'details') {
    Registry::set('navigation.tabs.shipping_info', array (
        'title' => __('shipping_info'),
        'js' => true
    ));
}
