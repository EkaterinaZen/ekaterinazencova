<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update_details') {
        
        $fields = array();
        
        if (isset($_REQUEST['update_order']['shipping_comment'])){
            $fields['shipping_comment'] = $_REQUEST['update_order']['shipping_comment'];
        }
        db_query("UPDATE ?:orders SET ?u WHERE order_id = ?i", $fields, $_REQUEST['order_id']);     
    }
}

if ($mode == 'details') {

    Registry::set('navigation.tabs.shipping_data', array (
        'title' => __('shipping_data'),
        'js' => true
    ));
}
