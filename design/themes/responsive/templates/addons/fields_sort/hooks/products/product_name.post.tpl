{if $selected_layout == "short_list"}
    {if $product.field_date}
        <div class="ty-control-group" id="elm_sort_field_date">
            <label for="elm_sort_field_date" class="ty-control-group__label" >{__("field_date")}:</label>
            <span class="ty-control-group__item" id="elm_sort_field_date">{$product.field_date|date_format:"`$settings.Appearance.date_format`"|default:$smarty.const.TIME}</span>
        </div>
    {/if}
    {if $product.field_location}
        <div class="ty-control-group" id="elm_sort_field_location">
            <label for="elm_sort_field_location" class="ty-control-group__label" >{__("field_location")}:</label>
            <span class="ty-control-group__item" id="elm_sort_field_location">{$product.field_location|fn_get_destination_name}</span>
        </div>
    {/if}
{/if}
