{if $product.custom_text}
    <div class="ty-control-group product-list-field">
        <label class="ty-control-group__label">{__("custom_text")}:</label>
        <span class="ty-qty-out-of-stock ty-control-group__item">{$product.custom_text nofilter}</span>
    </div>
{/if}
