{if $category_data.isbn || $category_data.author || $category_data.language || $category_data.format}
<div class="info">
    <h3>{__("books_data")}<h3>
    {if $category_data.isbn}
        <div class="ty-info-group">
            <span class="ty-control-group__label">{__("isbn")}:</span>
            <span class="ty-control-group__item">{$category_data.isbn}</span>
        </div>
    {/if}
    {if $category_data.author}
        <div class="ty-info-group">
            <span class="ty-control-group__label">{__("author")}:</span>
            <span class="ty-control-group__item">{$category_data.author}</span>
        </div>
    {/if}
    {if $category_data.language}
        <div class="ty-info-group">
            <span class="ty-control-group__label">{__("language")}:</span>
            <span class="ty-control-group__item">{$category_data.language}</span>
        </div>
    {/if}
    {if $category_data.format}
        <div class="ty-info-group">
            <span class="ty-control-group__label">{__("format")}:</span>
            <span class="ty-control-group__item">{$category_data.format}</span>
        </div>
    {/if}
</div>
{/if}
