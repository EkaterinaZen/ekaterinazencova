<div class="control-group cm-no-hide-input">
    <label class="control-label" for="elm_product_custom_text">{__("custom_text")}:</label>
    <div class="controls">
        <textarea id="elm_product_custom_text" name="product_data[custom_text]" cols="55" rows="8" class="cm-wysiwyg input-large">{$product_data.custom_text}</textarea>
    </div>
</div>

