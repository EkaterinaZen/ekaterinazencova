<div id="content_ending_date" class="hidden">
    <div class="control-group">
        <label class="control-label" for="elm_ending_date_holder">{__("ending_date")}:</label>
        <div class="controls">
            {include file="common/calendar.tpl" date_id="elm_ending_date_holder" date_name="product_data[ending_date]" date_val=$product_data.ending_date|default:"" start_year=$settings.Company.company_start_year}
        </div>
    </div>
</div>
