{if $entity_data}
    {assign var="id" value=$entity_data.entity_id}
{else}
    {assign var="id" value=0}
{/if}

{include file="addons/admin_page/views/admin_page/components/profiles_scripts.tpl"}

<form name="admin_page_form" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table">
{capture name="mainbox"}

{capture name="tabsbox"}
    <input type="hidden" name="entity_id" value="{$id}" />
    <input type="hidden" class="cm-no-hide-input" name="selected_section" id="selected_section" value="{$selected_section}" />
        
    <div id="content_general"> 
        <div class="control-group cm-no-hide-input">
            <label for="entity_title" class="control-label cm-required">{__("title")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="entity_data[title]" id="entity_title" size="55" value="{$entity_data.title}" />
            </div>
        </div>
        
          <div class="control-group cm-no-hide-input">
            <label for="entity_position" class="control-label cm-required">{__("position")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="entity_data[position]" id="entity_position" size="55" value="{$entity_data.position}" />
            </div>
        </div>
             
        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_entity_comment">{__("comment")}:</label>
            <div class="controls">
                <textarea id="elm_entity_comment" name="entity_data[comment]" cols="55" rows="8" class="cm-wysiwyg input-large">{$entity_data.comment}</textarea>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="elm_entity_date">{__("creation_date")}</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="elm_entity_date" date_name="entity_data[date]" date_val=$smarty.const.TIME start_year=$settings.Company.company_start_year}
            </div>
        </div>
           
        <div class="control-group">
            <label class="control-label" for="elm_entity_edit_date">{__("edit_date")}</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="elm_entity_edit_date" date_name="entity_data[edit_date]" date_val=$smarty.const.TIME start_year=$settings.Company.company_start_year}
            </div>
        </div>                  
          
        {include file="common/select_status.tpl" input_name="entity_data[status]" id="elm_entity_status" obj=$entity_data hidden=true}
    </div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$selected_section track=true}
{/capture}
{if !$id}
    {assign var="_title" value="{__("new_profile")}"}
{else}
    {assign var="_title" value="{__("editing_profile")}: `$entity_data.title`"}
{/if}

{$_title = $_title|strip_tags}
{assign var="redirect_url" value="admin_page.manage"}

{capture name="buttons"}
    {capture name="tools_list"}
        <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="admin_page.delete?entity_id=`$id`&redirect_url=`$redirect_url`"}</li>     
    {/capture}
    {if $id && $smarty.capture.tools_list|trim !==""}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
    <div class="btn-group btn-hover dropleft">
        {if $id}
            {include file="buttons/save_changes.tpl" but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[admin_page.`$runtime.mode`]" but_target_form="admin_page_form" save=$id}
        {else}
            {include file="buttons/button.tpl" but_text=__("create") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[admin_page.`$runtime.mode`]" but_target_form="admin_page_form" save=$id}
        {/if}
    </div>

{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
</form>
