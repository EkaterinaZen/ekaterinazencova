{if "MULTIVENDOR"|fn_allowed_for}
    {assign var="no_hide_input" value="cm-no-hide-input"}
{/if}

{include file="addons/admin_page/views/admin_page/components/profiles_scripts.tpl"}

{capture name="mainbox"}

{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

<form action="{""|fn_url}" method="post" name="entitylist_form" id="entitylist_form" class="{if $runtime.company_id && !"ULTIMATE"|fn_allowed_for}cm-hide-inputs{/if}">
<input type="hidden" name="fake" value="1" />

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{if $entities}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="1%" class="center {$no_hide_input}">
        {include file="common/check_items.tpl"}</th>
    <th width="3%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=title&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("title")}{if $search.sort_by == "title"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=edit_date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("edit_date")}{if $search.sort_by == "edit_date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    {hook name="admin_page:manage_header"}{/hook}
    <th width="20%" class="right">&nbsp;</th>
    <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>

</tr>
</thead>
{foreach from=$entities item=entity}

{assign var="allow_save" value=$entity|fn_allow_save_object:"entities"}

{*
{if !$allow_save && !"RESTRICTED_ADMIN"|defined && $auth.is_root != 'Y'}
    {assign var="link_text" value=__("view")}
    {assign var="popup_additional_class" value=""}
{elseif $allow_save || "RESTRICTED_ADMIN"|defined || $auth.is_root == 'Y'}
    {assign var="link_text" value=""}
    {assign var="popup_additional_class" value="cm-no-hide-input"}
{else}
    {assign var="popup_additional_class" value=""}
    {assign var="link_text" value=""}
{/if}
*}

{if "ULTIMATE"|fn_allowed_for}
    <tr class="cm-row-status-{$entity.status|lower}{if !$allow_save} cm-hide-inputs{/if}">
{/if}
    <td class="center {$no_hide_input}">
        <input type="checkbox" name="entity_ids[]" value="{$entity.entity_id}" class="checkbox cm-item" /></td>
    <td><a class="row-status" href="{"admin_page.update?entity_id=`$entity.entity_id`"|fn_url}">{$entity.entity_id}</a></td>
    <td class="row-status">{if $entity.title}<a href="{"admin_page.update?entity_id=`$entity.entity_id`"|fn_url}">{$entity.title}</a>{else}-{/if}</td>
    <td class="row-status">{$entity.date|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
    <td class="row-status">{$entity.edit_date|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
    {hook name="admin_page:manage_data"}{/hook}
    <td class="right nowrap">
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="admin_page.update?entity_id=`$entity.entity_id`"}</li>
            <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="admin_page.delete?entity_id=`$entity.entity_id`&redirect_url=`$return_current_url`"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
    <td class="right">
        {include file="common/select_popup.tpl" id=$entity.entity_id status=$entity.status hidden=true  update_controller="admin_page" notify=false popup_additional_class="`$popup_additional_class` dropleft" non_editable=false table="my_entites" object_id_name="entity_id"}
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="buttons"}
    {if $entities}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[admin_page.m_delete]" form="entitylist_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
{/capture}
</form>
{/capture}

{capture name="adv_buttons"}
    {assign var="_title" value=__("my_page")}
    
    {if !($runtime.company_id && "MULTIVENDOR"|fn_allowed_for)}
        <a class="btn cm-tooltip" href="{"admin_page.add"|fn_url}" title="{__("add_entity")}"><i class="icon-plus"></i></a>        
    {/if}
{/capture}

{capture name="sidebar"}
{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons content_id="manage_users"}
