<div id="content_orders_statistics">     
    <div id="dashboard_statistics_orders_statistics">  
        <div class="dashboard-table dashboard-table-orders_statistics">
            <div class="table-wrap" id="dashboard_orders_statistics">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="14%">{__("status")}</th>
                            <th width="13%">{__("this_week")}</th>
                            <th width="14%">{__("previous_week")}</th>
                            <th width="14%">{__("this_month")}</th>
                            <th width="14%">{__("previous_month")}</th>
                            <th width="13%">{__("this_year")}</th>
                            <th width="16%">{__("previous_year")}</th>                
                        </tr>
                    </thead>
                </table>
                <div class="scrollable-table">
                    <table class="table table-striped">
                        <tbody>
                            {foreach from=$orders_statistics key="status" item="order_statistics"}                 
                                <tr>
                                    <td width="14%">{$four_statuses.$status}</td>
                                    <td width="14%">
                                        {if ($order_statistics.this_week.count)} 
                                            {$order_statistics.this_week.count} {else}0
                                        {/if}
                                    </td>
                                    <td width="14%">
                                        {if ($order_statistics.last_week.count)} 
                                            {$order_statistics.last_week.count} {else}0
                                        {/if}
                                    </td>
                                    <td width="14%">
                                        {if ($order_statistics.this_month.count)} 
                                            {$order_statistics.this_month.count} {else}0
                                        {/if}
                                    </td>
                                    <td width="14%">
                                        {if ($order_statistics.last_month.count)} 
                                            {$order_statistics.last_month.count} {else}0
                                        {/if}
                                    </td>                                  
                                    <td width="13%">
                                        {if ($order_statistics.this_year.count)} 
                                            {$order_statistics.this_year.count} {else}0
                                        {/if}
                                    </td>
                                    <td width="16%">
                                        {if ($order_statistics.last_year.count)} 
                                            {$order_statistics.last_year.count} {else}0
                                        {/if}
                                    </td>                                                     
                                </tr>
                            {/foreach}
                            <tr> 
                                <td width="14%"><b>{__('gross_total')}</b></td>                               
                                <td width="14%">
                                    {if ($gross_total.this_week)} 
                                        {include file="common/price.tpl" value=$gross_total.this_week} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($gross_total.last_week)} 
                                        {include file="common/price.tpl" value=$gross_total.last_week} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($gross_total.this_month)} 
                                        {include file="common/price.tpl" value=$gross_total.this_month} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($gross_total.last_month)} 
                                        {include file="common/price.tpl" value=$gross_total.last_month} {else}0
                                    {/if}
                                </td>
                                <td width="13%">
                                    {if ($gross_total.this_year)} 
                                        {include file="common/price.tpl" value=$gross_total.this_year} {else}0
                                    {/if}
                                </td>
                                <td width="16%">
                                    {if ($gross_total.last_year)} 
                                        {include file="common/price.tpl" value=$gross_total.last_year} {else}0
                                    {/if}
                                </td>                              
                            </tr>
                            <tr> 
                                <td width="14%"><b>{__('totally_paid')}</b></td>                               
                                <td width="14%">
                                    {if ($total_paid.this_week)} 
                                        {include file="common/price.tpl" value=$total_paid.this_week} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($total_paid.last_week)} 
                                        {include file="common/price.tpl" value=$total_paid.last_week} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($total_paid.this_month)} 
                                        {include file="common/price.tpl" value=$total_paid.this_month} {else}0
                                    {/if}
                                </td>
                                <td width="14%">
                                    {if ($total_paid.last_month)} 
                                        {include file="common/price.tpl" value=$total_paid.last_month} {else}0
                                    {/if}
                                </td>
                                <td width="13%">
                                    {if ($total_paid.this_year)} 
                                        {include file="common/price.tpl" value=$total_paid.this_year} {else}0
                                    {/if}
                                </td>
                                <td width="16%">
                                    {if ($total_paid.last_year)} 
                                        {include file="common/price.tpl" value=$total_paid.last_year} {else}0
                                    {/if}
                                </td>                              
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
