<div id="content_shipping_info" class="hidden">

    {include file="common/mainbox.tpl" title=__("upc") target="#shipping_info"}

    <div id="shipping_info" class="collapse in">
        <div class="control-group">
            <label class="control-label" for="shipping_date">{__("shipping_date")}:</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="shipping_date" date_name="product_data[shipping_date]" date_val=$product_data.shipping_date|default:"Y-m-d"|date start_year=$settings.Company.company_start_year}
            </div>
        </div>
        
        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_product_shipping_comment">{__("shipping_comment")}:</label>
            <div class="controls">
                <textarea id="elm_shipping_comment" name="product_data[shipping_comment]" cols="55" rows="8" class="cm-wysiwyg input-large">{$product_data.shipping_comment}</textarea>
            </div>
        </div>
    </div>
</div>
