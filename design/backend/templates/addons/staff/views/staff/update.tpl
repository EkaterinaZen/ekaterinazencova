{if $staff_data}
    {assign var="id" value=$staff_data.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{include file="addons/staff/views/staff/components/profiles_scripts.tpl"}

<form name="staff_form" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table" enctype="multipart/form-data">
{capture name="mainbox"}

{capture name="tabsbox"}
    <input type="hidden" name="staff_id" value="{$id}" />
    <input type="hidden" class="cm-no-hide-input" name="selected_section" id="selected_section" value="{$selected_section}" />
        
    <div id="content_general"> 
        <div class="control-group cm-no-hide-input">
            <label for="first_name" class="control-label">{__("first_name")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[first_name]" id="first_name" size="55" value="{$staff_data.first_name}" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="last_name" class="control-label">{__("last_name")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[last_name]" id="last_name" size="55" value="{$staff_data.last_name}" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="email" class="control-label">{__("email")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[email]" id="email" size="55" value="{$staff_data.email}" />
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label for="function" class="control-label">{__("staff_function")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="staff_data[function]" id="last_name" size="55" value="{$staff_data.function}" />
            </div>
        </div>		
		<div class="control-group">
			<label class="control-label" for="user_id">{__("user_id")}</label>
			<div class="controls">
				<select class="span5" name="staff_data[user_id]" id="user_id">
				    <option value="">-</option>
					{foreach from=$users item="user"}
						<option value="{$user.user_id}" {if $user.user_id == $staff_data.user_id}selected="selected"{/if}>{$user.user_id|fn_get_user_name}</option>
					{/foreach}
				</select>
			</div> 
		</div>
		<div class="control-group">
			<label class="control-label">{__("images")}:</label>
			<div class="controls">
				{include file="common/attach_images.tpl" image_name="staff_main" image_object_type="staff" image_pair=$staff_data.main_pair icon_text=__("text_staff_thumbnail") detailed_text=__("text_staff_detailed_image") no_thumbnail=true}
			</div>
		</div>		
		{include file="common/select_status.tpl" input_name="staff_data[status]" id="elm_staff_status" obj=$staff_data hidden=true}
    </div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$selected_section track=true}
{/capture}
{if !$id}
    {assign var="_title" value="{__("new_staff")}"}
{else}
    {assign var="_title" value="{__("editing_staff")}: `$staff_data.first_name` `$staff_data.last_name`"}
{/if}

{assign var="redirect_url" value="staff.manage"}

{capture name="buttons"}
    {capture name="tools_list"}
        <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="staff.delete?staff_id=`$id`&redirect_url=`$redirect_url`"}</li>     
    {/capture}
    {if $id && $smarty.capture.tools_list|trim !==""}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
    <div class="btn-group btn-hover dropleft">
        {if $id}
            {include file="buttons/save_changes.tpl" but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_form" save=$id}
        {else}
            {include file="buttons/button.tpl" but_text=__("create") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_form" save=$id}
        {/if}
    </div>

{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
</form>
