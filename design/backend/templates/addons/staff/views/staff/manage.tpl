{include file="addons/staff/views/staff/components/profiles_scripts.tpl"}

{capture name="mainbox"}

{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

<form action="{""|fn_url}" method="post" name="staff_list_form" id="staff_list_form" class="{if $runtime.company_id && !"ULTIMATE"|fn_allowed_for}cm-hide-inputs{/if}">
<input type="hidden" name="fake" value="1" />

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{$c_url|fn_print_die}

{if $staff_data}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="1%" class="center {$no_hide_input}">
        {include file="common/check_items.tpl"}</th>
    <th width="3%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=first_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff_name")}{if $search.sort_by == "first_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff_user_id")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    {hook name="staff:manage_header"}{/hook}
    <th width="20%" class="right">&nbsp;</th>
    <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>

</tr>
</thead>
{foreach from=$staff_data item=staff}

{assign var="allow_save" value=$staff|fn_allow_save_object:"staff_data"}

{*
{if !$allow_save && !"RESTRICTED_ADMIN"|defined && $auth.is_root != 'Y'}
    {assign var="link_text" value=__("view")}
    {assign var="popup_additional_class" value=""}
{elseif $allow_save || "RESTRICTED_ADMIN"|defined || $auth.is_root == 'Y'}
    {assign var="link_text" value=""}
    {assign var="popup_additional_class" value="cm-no-hide-input"}
{else}
    {assign var="popup_additional_class" value=""}
    {assign var="link_text" value=""}
{/if}
*}

{if "ULTIMATE"|fn_allowed_for}
    <tr class="cm-row-status-{$staff.status|lower}{if !$allow_save} cm-hide-inputs{/if}">
{/if}
    <td class="center {$no_hide_input}">
        <input type="checkbox" name="staff_ids[]" value="{$staff.staff_id}" class="checkbox cm-item" /></td>
    <td><a class="row-status" href="{"staff.update?staff_id=`$staff.staff_id`"|fn_url}">{$staff.staff_id}</a></td>
    <td class="row-status">{if $staff.first_name}<a href="{"staff.update?staff_id=`$staff.staff_id`"|fn_url}">{$staff.first_name} {$staff.last_name}</a>{else}-{/if}</td>
    <td class="row-status">{if $staff.email}{$staff.email}{else}-{/if}</td>
    <td class="row-status">{if $staff.function}{$staff.function}{else}-{/if}</td>
    <td class="row-status">{if $staff.user_id}<a href='{"profiles.update&user_id=`$staff.user_id`"|fn_url}'>{$staff.user_id|fn_get_user_name}</a>{else}-{/if}</td>
    {hook name="staff:manage_data"}{/hook}
    <td class="right nowrap">
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff.update?staff_id=`$staff.staff_id`"}</li>
            <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="staff.delete?staff_id=`$staff.staff_id`&redirect_url=`$return_current_url`"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
    <td class="right">
        {include file="common/select_popup.tpl" id=$staff.staff_id status=$staff.status hidden=true  update_controller="staff" notify=false popup_additional_class="`$popup_additional_class` dropleft" non_editable=false table="staff" object_id_name="staff_id"}
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}


{capture name="buttons"}
    {if $staff_data}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="staff_list_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
{/capture}
</form>
{/capture}

{capture name="adv_buttons"}
    {assign var="_title" value=__("staff_members")}
    
    {if !($runtime.company_id && "MULTIVENDOR"|fn_allowed_for)}
        <a class="btn cm-tooltip" href="{"staff.add"|fn_url}" title="{__("add_staff")}"><i class="icon-plus"></i></a>        
    {/if}
{/capture}

{capture name="sidebar"}
{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_staff"}
