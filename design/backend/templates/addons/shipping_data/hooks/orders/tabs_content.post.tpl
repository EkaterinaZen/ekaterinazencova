<div id="content_shipping_info" class="hidden">
  
    {include file="common/subheader.tpl" title=__("shipping_info") target="#shipping_data"}

    <div id="shipping_data" class="collapse in">
        <div class="control-group">
            <label class="control-label" for="shipping_date">{__("shipping_date")}:</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="shipping_date" date_name="update_order[shipping_date]" date_val=$order_info.shipping_date|default:"Y-m-d"|date start_year=$settings.Company.company_start_year}
            </div>
        </div>
        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_shipping_comment">{__("shipping_comment")}:</label>
            <div class="controls">
                <textarea id="elm_shipping_comment" name="update_order[shipping_comment]" cols="55" rows="8" class="cm-wysiwyg input-large">{$order_info.shipping_comment}</textarea>
            </div>
        </div>
    </div>
</div>
