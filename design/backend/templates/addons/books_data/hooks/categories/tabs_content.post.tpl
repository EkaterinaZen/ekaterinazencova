<div id="content_books_data" class="hidden">

    {include file="common/subheader.tpl" title=__("book") target="#acc_book"}

    <div id="acc_book" class="collapse in">
        <div class="control-group {$no_hide_input_if_shared_product}">
            <label for="books_isbn" class="control-label">{__("isbn")}</label>
            <div class="controls">
                <input class="input-text-short" type="text" name="category_data[isbn]" id="books_isbn" size="20" value="{$category_data.isbn}" />
            </div>
        </div>

        <div class="control-group {$no_hide_input_if_shared_product}">
            <label for="books_author" class="control-label">{__("author")}</label>
            <div class="controls">
                <input class="input-large" type="text" name="category_data[author]" id="books_author" size="20" value="{$category_data.author}" />
            </div>
        </div>

        <div class="control-group {$no_hide_input_if_shared_product}">
            <label for="books_language" class="control-label">{__("language")}</label>
            <div class="controls">
                <input class="input-text-short" type="text" name="category_data[language]" id="books_language" size="20" value="{$category_data.language}" />
            </div>
        </div>

        <div class="control-group {$no_hide_input_if_shared_product}">
            <label for="books_format" class="control-label">{__("format")}</label>
            <div class="controls">
                <input class="input-text-short" type="text" name="category_data[format]" id="books_format" size="20" value="{$category_data.format}" />
            </div>
        </div>
    </div>
</div>
